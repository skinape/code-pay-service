package cn;
import java.net.URLDecoder;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;
import cn.service.impl.HttpUtils;
import cn.util.EncryptUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Test {

    public static void main(String[] arg) throws Exception {
        for (int i=0;i<30;i++){


//        Map<String,Object> order=new HashMap<>() ;
//
//        order.put("sktype",2);
//        order.put("skgdtype",1);
//        order.put("skmoney",0.47);
//        order.put("manongkey",2);
//        order.put("userbh","wwww");
//        order.put("skordernum","202005181557402710");
//        order.put("time","2019-09-18");
//
//       System.out.println(HttpUtils.post("http://fan.baldhome.com/notify.php", order))  ;
//
        String response="";
        String postid="SF1312336874607";
        String okenV2="";
        String com="";
        Map<String, String> header=new HashMap<>();
        String ip="122.227.209."+(int)(Math.random()*900)+100;
        header.put("Client-Ip",ip);
        header.put("X-Forwarded-For",ip);
        header.put("User-Agent","Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.80 Safari/537.36 Core/1.47.277.400 QQBrowser/9.4.7658.400");
        header.put("cookie","BIDUPSID=132FB974F9FF486ADDACC0A5A712EEF7; PSTM=1602568133; BAIDUID=132FB974F9FF486ADEB71892F0682036:FG=1; BDUSS=2dEdTlPb3FqRkw3eX5MNW1kLWZDLS1yQjhnUm5BQkJ2UDE1OUpXQjdSY2xVd1ZnRVFBQUFBJCQAAAAAAAAAAAEAAAB6-Ho9WUlNT0hPTkdDSEVOR18AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACXG3V8lxt1fZ; BDUSS_BFESS=2dEdTlPb3FqRkw3eX5MNW1kLWZDLS1yQjhnUm5BQkJ2UDE1OUpXQjdSY2xVd1ZnRVFBQUFBJCQAAAAAAAAAAAEAAAB6-Ho9WUlNT0hPTkdDSEVOR18AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACXG3V8lxt1fZ; BAIDUID_BFESS=132FB974F9FF486ADEB71892F0682036:FG=1; __yjs_duid=1_bd9c326e63693f1de3ea9b26395e6d201608861298888; ab_sr=1.0.0_ZDMzMjQ5ZGVlMmEzNGI1OTQ1YzFjZGZlYzk0YzU3YjMwOTUyYTU4MzAwY2FlNjRlMGQzZWJkYWJhYzJlNDM1MjY4ZTU3YTZiNjhiNWZmZmFiOGY0YjFjNTkzMTlhZjg5Mjc2OWJjMGRhYTlhM2Y4ZjA0MzM1OTA4NTllNWZmYzM=; BDRCVFR[tiixOo0cjw_]=mk3SLVN4HKm; delPer=0; PSINO=6; BDORZ=FFFB88E999055A3F8A630C64834BD6D0; H_PS_PSSID=1448_33222_33306_33235_31253_33343_33313_33312_33311_33310_33309_33321_33308_33307_33144_33265; BDRCVFR[feWj1Vr5u3D]=I67x6TjHwwYf0; BA_HECTOR=858k8h25000l0k25ki1fualg80r");
        response=HttpUtils.get("https://www.baidu.com/s?ie=utf-8&f=8&rsv_bp=1&rsv_idx=2&ch=&tn=baiduhome_pg&bar=&wd="+postid+"&rsv_spt=1&oq=550009521127898&rsv_pq=895a832c000005a6&rsv_t=1fe9ETqNVYCwTixSlpZwA4cFEhogS76H6r2ZkG8bfSCrMB%2Fr5u6qlh0%2B9kye14yHe%2FEZ&rqlang=cn&rsv_btype=t&rsv_dl=tb", header);
        okenV2=getSubString(response,"okenV2=","\"");
        com=getSubString(response,"com:'","'");
        response=HttpUtils.get("https://express.baidu.com/express/api/express?query_from_srcid=&isBaiduBoxApp=10002&isWisePc=10020&tokenV2="+okenV2+"&cb=jQuery1102006219520766524633_1608865287656&appid=10002&com="+com+"&nu="+postid+"&vcode=&token=&qid=8ed20b24000179c9&_=1608865287660", header);

        response="{"+getSubString(response,"({","})")+"}";

        System.out.println(JSON.toJSONString(JSON.parseObject(response), SerializerFeature.PrettyFormat))  ;
        }
    }
    /**
     * 取两个文本之间的文本值
     * @param text 源文本 比如：欲取全文本为 12345
     * @param left 文本前面
     * @param right 后面文本
     * @return 返回 String
     */
    public static String getSubString(String text, String left, String right) {
        String result = "";
        int zLen;
        if (left == null || left.isEmpty()) {
            zLen = 0;
        } else {
            zLen = text.indexOf(left);
            if (zLen > -1) {
                zLen += left.length();
            } else {
                zLen = 0;
            }
        }
        int yLen = text.indexOf(right, zLen);
        if (yLen < 0 || right == null || right.isEmpty()) {
            yLen = text.length();
        }
        result = text.substring(zLen, yLen);
        return result;
    }
}
