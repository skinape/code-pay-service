package cn.mapper;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface MryeMapper {

    /**
     * 通用查询方法(分页)
     *@Param("sql") String sqlStr, @Param("pageSize") int pageSize, @Param("pageNo") int pageNo
     * @param
     * @return
     */
    List<Map<String, Object>> searchSapPage(Map<String, Object> params);

    /**
     * 通用查询方法
     *@Param("sql") String sql  和参数
     * @param params
     * @return
     */
    List<Map<String, Object>> searchSapList(Map<String, Object> params);
    /**
     * 通用单个查询方法
     *@Param("sql") String sql  和参数
     * @param params
     * @return
     */
    Map<String, Object> searchSap(Map<String, Object> params);

    /**
     * 通用保存sap数据库
     *
     * @param params
     */
    int saveSap(Map<String, Object> params);

    /**
     * 通用删除sap数据库
     *
     * @param params
     */
    int deleteSap(Map<String, Object> params);

    /**
     * 通用修改sap数据库
     *
     * @param params
     */
    int updateSap(Map<String, Object> params);

    /**
     * 通用批量添加数据库
     *
     * @param params
     * @param values
     */
    int saveBatchSap(@Param("params") Map<String, Object> params, @Param("list") List<Map<String, Object>> values);

    /**
     * 通用批量修改  带 in
     *
     * @param params
     * @param inValues
     */
    int updateBatchSap(@Param("params") Map<String, Object> params, @Param("inValues") List<String> inValues);

    /**
     * 执行存储过程
     * @param map
     */
    int updateProc(Map<String, Object> map);

    Object getProc(Map<String, Object> map);

}
