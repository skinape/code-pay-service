package cn.util;

import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import java.util.Map;

public class ServiceResult {

    public static Object createResult(Model model,Map map,Object result){
        //返回JSON 数据
        if(map.containsKey("resultype")&&map.get("resultype").equals("json")){
            ModelAndView mav = new ModelAndView(new MappingJackson2JsonView());
            mav.addObject("result", result);
            return mav;
        }else if (map.containsKey("resultype")&&map.get("resultype").equals("string")){
            //返回字符串
            return result;
        }
        else{
            ModelAndView mav = new ModelAndView((String) map.get("path"));
            //返回页面
            model.addAttribute("result",result);
            return mav;
        }
    }
}
