package cn.util.shiro;

import org.apache.shiro.session.SessionException;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.LogoutFilter;
import org.springframework.stereotype.Service;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * Shiro退出
 */
@Service("systemLogoutFilter")
public class SystemLogoutFilter extends  LogoutFilter{
	@Override
	 protected boolean preHandle(ServletRequest request, ServletResponse response) throws Exception {
     //退出过滤器
	  Subject subject = getSubject(request, response);
	 
	  String redirectUrl = getRedirectUrl(request, response, subject);
	 
	  try {
	 
	   subject.logout();
	 
	  } catch (SessionException ise) {
	 
	   ise.printStackTrace();
	 
	  }
	  issueRedirect(request, response, redirectUrl);

	  return false;
	 }
}
