package cn.util;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

public class MapUtil {

    public static Map<String,Object> chuli(String json){
        try {
            //url传来的 参数去除编码
            json=URLDecoder.decode(json, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        StringBuffer result=new StringBuffer("{");
        Map<String,Object> map= new HashMap<>();
        String [] str1;
        String [] str2;
        str1=json.split("&");
        for (String str3:str1){
            //str2=str3.split("=");
            result.append("\""+str3.substring(0,str3.indexOf("="))+"\":"+"\""+str3.substring(str3.indexOf("=")+1)+"\",");
        }
        result.deleteCharAt(result.length()-1);
        result.append("}");
        try {
            map=JacksonUtil.parseJson(result.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }
}
