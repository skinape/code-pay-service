package cn.controller;

import cn.service.CodePayUserService;
import cn.util.JacksonUtil;
import cn.util.ServiceResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/user")
public class CodePayUserController {

    @Resource
    CodePayUserService codePayUserService;

    private Logger logger=LoggerFactory.getLogger(CodePayUserController.class);


    @RequestMapping("/login.html")
    public Object login(@RequestBody String json, Model model){
        logger.info("访问地址===login.html;访问参数==="+json);
        //返回数据
        Map<String,Object> result=new HashMap<>();
        //接收数据
        Map<String,Object> map = new HashMap<>();
        //返回模板
        map.put("path","user/login");
        return ServiceResult.createResult(model,map,result);
    }

    @RequestMapping("/reg.html")
    public Object reg(@RequestBody String json, Model model){
        logger.info("访问地址===reg.html;访问参数==="+json);
        //返回数据
        Map<String,Object> result=new HashMap<>();
        //接收数据
        Map<String,Object> map = new HashMap<>();
        //返回模板
        map.put("path","user/reg");
        return ServiceResult.createResult(model,map,result);
    }

    /**
     * 注册
     * @param json
     * @param model
     * @return
     */
    @RequestMapping("/reg.json")
    @ResponseBody
    public Object reginfo(@RequestBody String json, Model model){
        logger.info("访问地址===reg.json;访问参数==="+json);
        //返回数据
        Object result=0;
        //接收数据
        Map<String,Object> map = new HashMap<>();
        try {
            map=JacksonUtil.parseJson(json);
            result=codePayUserService.reginfo(map);
            map.put("resultype","string");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ServiceResult.createResult(model,map,result);
    }

    /**
     * 获取用户名是否存在
     * @param json
     * @param model
     * @return
     */
    @RequestMapping("/checkLoginName.json")
    @ResponseBody
    public Object checkLoginName(@RequestBody String json, Model model){
        //logger.info("访问地址===reg.html;访问参数==="+json);
        //返回数据
        Object result=0;
        //接收数据
        Map<String,Object> map = new HashMap<>();
        try {
            map=JacksonUtil.parseJson(json);
            result=codePayUserService.checkLoginName(map);
            map.put("resultype","string");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ServiceResult.createResult(model,map,result);
    }


    @RequestMapping({"/","/index.html"})
    public Object index(@RequestBody String json, Model model){
        logger.info("访问地址===index;访问参数==="+json);
        //返回数据
        Map<String,Object> result=new HashMap<>();
        //接收数据
        Map<String,Object> map = new HashMap<>();
        map.put("start",0);
        map.put("end",20);
        //个人信息
        result.put("userinfo",codePayUserService.getuserinfo(map));
        //获取域名信息
        result.put("urlinfo",codePayUserService.geturlinfo(map));
        //获取收款信息
        result.put("receivables",codePayUserService.getreceivables(map));
        //获取今日 昨日 一周 所有  收款金额
        result.put("timemoney",codePayUserService.gettimemoney(map));
        //返回模板
        map.put("path","user/index");
        return ServiceResult.createResult(model,map,result);
    }

    @RequestMapping("/profit.html")
    public Object profit(@RequestBody String json, @RequestParam("sktype") Integer sktype, @RequestParam("start")  Integer start, Model model){
        logger.info("访问地址===profit.html;访问参数==="+json);
        //返回数据
        Map<String,Object> result=new HashMap<>();
        //接收数据
        Map<String,Object> map = new HashMap<>();
        map.put("sktype",sktype);
        map.put("start",start);
        map.put("end",20);
        //个人信息
        result.put("userinfo",codePayUserService.getuserinfo(map));
        //获取收款信息
        result.put("receivables",codePayUserService.getreceivables(map));
        result.put("upper",start-20<=0?0:start-20);
        result.put("lower",start+20);
        result.put("sktype",sktype);
        //返回模板
        map.put("path","user/profit");
        return ServiceResult.createResult(model,map,result);
    }
    @RequestMapping("/urls.html")
    public Object urls(@RequestBody String json, Model model){
        logger.info("访问地址===urls.html;访问参数==="+json);
        //返回数据
        Map<String,Object> result=new HashMap<>();
        //接收数据
        Map<String,Object> map = new HashMap<>();
        //个人信息
        result.put("userinfo",codePayUserService.getuserinfo(map));
        //获取域名信息
        result.put("urlinfo",codePayUserService.geturlinfo(map));
        //返回模板
        map.put("path","user/urls");
        return ServiceResult.createResult(model,map,result);
    }
    @RequestMapping("/url-add.html")
    public Object urladdhtml(@RequestBody String json, @RequestParam(value = "urlid",required = false) Integer urlid,Model model){
        logger.info("访问地址===url-add.html;访问参数==="+json);
        //返回数据
        Map<String,Object> result=new HashMap<>();
        //接收数据
        Map<String,Object> map = new HashMap<>();
        map.put("urlid",urlid);
        if (urlid!=null){
            //获取单个域名信息
            result.put("urlinfo",codePayUserService.geturlinfo(map).get(0));
        }
        //返回模板
        map.put("path","user/url-add");
        return ServiceResult.createResult(model,map,result);
    }
    //新增域名
    @RequestMapping("/weihuurl.json")
    @ResponseBody
    public Object weihuurl(@RequestBody String json, Model model){
        logger.info("访问地址===weihuurl.json;访问参数==="+json);
        //返回数据
        Object result="";
        //接收数据
        Map<String,Object> map = new HashMap<>();
        try {
            map=JacksonUtil.parseJson(json);
            map.put("resultype","string");
        } catch (Exception e) {
            e.printStackTrace();
        }

        //新增域名
        result=codePayUserService.weihuurl(map);
        return ServiceResult.createResult(model,map,result);
    }

    //删除域名
    @RequestMapping("/daleteurl.json")
    @ResponseBody
    public Object daleteurl(@RequestBody String json, Model model){
        logger.info("访问地址===daleteurl.json;访问参数==="+json);
        //返回数据
        Object result="";
        //接收数据
        Map<String,Object> map = new HashMap<>();
        try {
            map=JacksonUtil.parseJson(json);
            map.put("resultype","string");
        } catch (Exception e) {
            e.printStackTrace();
        }

        //删除域名
        result=codePayUserService.daleteurl(map);
        return ServiceResult.createResult(model,map,result);
    }
    //进入个人认证
    @RequestMapping("/certcenter.html")
    public Object certcenter(@RequestBody String json, Model model){
        logger.info("访问地址===certcenter.html;访问参数==="+json);
        //返回数据
        Map<String,Object> result=new HashMap<>();
        //接收数据
        Map<String,Object> map = new HashMap<>();
        //个人信息
        result.put("userinfo",codePayUserService.getuserinfo(map));
        //返回模板
        map.put("path","user/certcenter");
        return ServiceResult.createResult(model,map,result);
    }

    //维护个人认证
    @RequestMapping("/updatecertcenter.json")
    public Object updatecertcenter(@RequestParam("name")String username, @RequestParam("tcard")String usertcard, @RequestParam("phone")String userphone, @RequestParam("imgfile") MultipartFile[] imgfile, Model model,HttpServletRequest request){
        logger.info("访问地址===certcenter.json");
        //返回数据
        Map<String,Object> result=new HashMap<>();
        //接收数据
        Map<String,Object> map = new HashMap<>();
        map.put("username",username);
        map.put("usertcard",usertcard);
        map.put("userphone",userphone);
        map.put("imgfile",imgfile);
        codePayUserService.updatecertcenter(map,request);
        //返回模板
        map.put("path","redirect:certcenter.html");
        return ServiceResult.createResult(model,map,result);
    }


}
