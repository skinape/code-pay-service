package cn.controller;

import CodePay.Pay;
import cn.service.CodePayHomeService;
import cn.util.JacksonUtil;
import cn.util.MapUtil;
import cn.util.ServiceResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Controller
public class CodePayHomeController {
    private Logger logger=LoggerFactory.getLogger(CodePayHomeController.class);

    @Resource
    CodePayHomeService codePayHomeService;



    @RequestMapping("/index.html")
    public Object index(@RequestBody String json, Model model){
        logger.info("访问地址===index.html;访问参数==="+json);
        //返回数据
        Map<String,Object> result=new HashMap<>();
        //接收数据
        Map<String,Object> map = new HashMap<>();
        //返回模板
        map.put("path","index");
        return ServiceResult.createResult(model,map,result);
    }
    @RequestMapping("/doc.html")
    public Object doc(@RequestBody String json, Model model){
        logger.info("访问地址===doc.html;访问参数==="+json);
        //返回数据
        Map<String,Object> result=new HashMap<>();
        //接收数据
        Map<String,Object> map = new HashMap<>();
        //返回模板
        map.put("path","doc");
        return ServiceResult.createResult(model,map,result);
    }
    @RequestMapping("/demo.html")
    public Object demo(@RequestBody String json, Model model){
        logger.info("访问地址===demo.html;访问参数==="+json);
        //返回数据
        Map<String,Object> result=new HashMap<>();
        //接收数据
        Map<String,Object> map = new HashMap<>();
        //返回模板
        map.put("path","demo");
        return ServiceResult.createResult(model,map,result);
    }

    @RequestMapping("/order.html")
    public Object order(@RequestBody String json, Model model){
        logger.info("访问地址===order.html;访问参数==="+json);
        //返回数据
        Map<String,Object> result=new HashMap<>();
        //接收数据
        Map<String,Object> map = new HashMap<>();
        //返回模板
        map.put("path","order");
        return ServiceResult.createResult(model,map,result);
    }




    /**
     * 创建订单
     * @param json
     * @param model
     * @return
     */
    @RequestMapping("/submitorder.json")
    public Object submitorder(@RequestBody String json, Model model){
        logger.info("访问地址===submitorder.json;访问参数==="+json);
        //接收数据
        Map<String,Object> map = null;
        //返回数据
        Map<String,Object> result=new HashMap<>();
        try {
            map = JacksonUtil.parseJson(json);
            //返回json数据
            map.put("resultype","json");
            //增值高低
            //map.put("gdtype",1);
            //返回可用金额
            result=codePayHomeService.submitorder(map);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ServiceResult.createResult(model,map,result);
    }

    /**
     * 获取你数据库里的订单信息
     * @param json
     * @param model
     * @return
     */
    @RequestMapping("/getorderstate.json")
    public Object getorderstate(@RequestBody String json, Model model){
        //logger.info("访问地址===getorderstate.json;访问参数==="+json);
        //接收数据
        Map<String,Object> map = null;
        //返回数据
        Map<String,Object> result=new HashMap<>();
        try {
            map = JacksonUtil.parseJson(json);
            //返回json数据
            map.put("resultype","json");
            //返回可用金额
            result=codePayHomeService.getorderstate(map);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ServiceResult.createResult(model,map,result);
    }

    /**
     * 码农服务器回调
     * @param json
     * @param model
     * @return
     */
    @RequestMapping("/setorderstate.json")
    public Object setorderstate(@RequestBody String json, Model model){
        logger.info("访问地址===setorderstate.json;访问参数==="+json);
        //接收数据
        Map<String,Object> map = null;
        //返回数据
        Map<String,Object> result=new HashMap<>();
        try {
            map = MapUtil.chuli(json);
            //返回json数据
            map.put("resultype","json");
            //返回状态
            result.put("state",codePayHomeService.setorderstate(map));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ServiceResult.createResult(model,map,result);
    }


}
