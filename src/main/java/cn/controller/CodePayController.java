package cn.controller;

import cn.service.CodePayService;
import cn.util.JacksonUtil;
import cn.util.MapUtil;
import cn.util.ServiceResult;
import com.alibaba.fastjson.JSON;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@Controller
public class CodePayController {
    @Resource
    CodePayService codePayService;
    private Logger logger=LoggerFactory.getLogger(CodePayController.class);
    /**
     * 登陆接口
     * @param json
     * @param model
     * @param request
     * @return
     */
    @RequestMapping("/login.json")
    public Object login(@RequestBody String json, Model model, HttpServletRequest request) throws Exception {
        logger.info("访问地址===login.json;访问参数==="+json);
        Map<String,Object> result=new HashMap<>();
        Map<String,Object> map = new HashMap<>();
        try {
            map = MapUtil.chuli(json);

        } catch (Exception e) {
            try {
                String userbh= request.getParameter("userbh");
                String userpwd= request.getParameter("userpwd");
                String service= request.getParameter("service");
                String resultype=request.getParameter("resultype");
                map.put("resultype",resultype);
                map.put("service",service);
                map.put("userbh",userbh);
                map.put("userpwd",userpwd);
            } catch (Exception e1) {
                map = JacksonUtil.parseJson(json);
            }
        }
        result=codePayService.login(map);
        return ServiceResult.createResult(model,map,result);
    }
    /**
     * 获取收款信息
     * @param json
     * @param model
     * @param request
     * @return
     */
    @RequestMapping("/gettimemoney.json")
    public Object getreceivables(@RequestBody String json, Model model, HttpServletRequest request, HttpServletResponse response){
        logger.info("访问地址===gettimemoney.json;访问参数==="+json);
        Map<String,Object> result=new HashMap<>();
        Map<String,Object> map = null;
        try {
            map = MapUtil.chuli(json);
            result.put("timemoney",codePayService.gettimemoney(map));
        } catch (Exception e) {
           e.printStackTrace();
        }

        return ServiceResult.createResult(model,map,result);
    }

    /**
     * 获取域名信息
     * @param json
     * @param model
     * @param request
     * @return
     */
    @RequestMapping("/geturl.json")
    public Object geturl(@RequestBody String json, Model model, HttpServletRequest request){
        logger.info("访问地址===geturl.json;访问参数==="+json);
        Map<String,Object> result=new HashMap<>();
        Map<String,Object> map = null;
        try {
            map = MapUtil.chuli(json);
            result.put("url", codePayService.geturl(map));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ServiceResult.createResult(model,map,result);
    }

    /**
     * 更新 收款单状态
     * @param json
     * @param model
     * @param request
     * @return
     */
    @RequestMapping("/updatereceivables.json")
    public Object addreceivables(@RequestBody String json, Model model, HttpServletRequest request){
        logger.info("访问地址===updatereceivables.json;访问参数==="+json);
        Map<String,Object> result=new HashMap<>();
        Map<String,Object> map = null;
        try {
            map = MapUtil.chuli(json);
            result.put("state", codePayService.updatereceivables(map));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ServiceResult.createResult(model,map,result);
    }


    /**
     * 获取可用金额   代码废弃
     * 参数:type 1.优惠价格 2.惩罚价格 , userid,支付类型,urlid,money 当前支付价格
     * 返回:0.01到0.99 可用的金额
     */
//    @RequestMapping("/getmoney.json")
//    @ResponseBody
//    public String getmoney(@RequestBody String json, HttpServletRequest request) throws Exception {
//        logger.info("访问地址===getmoney.json;访问参数==="+json);
//        String money="";
//        Map<String,Object> map = new HashMap<>();
//        try {
//            map = MapUtil.chuli(json);
//
//        } catch (Exception e) {
//            try {
//                String userbh= request.getParameter("userbh");
//                String urltype= request.getParameter("urltype");
//                String getmoney= request.getParameter("money");
//                String paytype= request.getParameter("paytype");
//                String gdtype= request.getParameter("gdtype");
//                String service= request.getParameter("service");
//                String resultype=request.getParameter("resultype");
//                map.put("resultype",resultype);
//                map.put("service",service);
//                map.put("userbh",userbh);
//                map.put("urltype",urltype);
//                map.put("money",getmoney);
//                map.put("paytype",paytype);
//                map.put("gdtype",gdtype);
//            } catch (Exception e1) {
//                map = JacksonUtil.parseJson(json);
//
//            }
//        }
//        money=codePayService.getmoney(map);
//        return money;
//    }

    /**
     * 创建订单
     * 参数:订单号  基本数据(userid，支付类型，urlid)
     *     金额
     *     type 1.优惠价格 2.惩罚价格
     *     支付类型
     */
    @RequestMapping("/createorder.json")
    @ResponseBody
    public Object createorder(@RequestBody String json,  Model model,HttpServletRequest request) throws Exception {
        logger.info("访问地址===createorder.json;访问参数==="+json);
        Map<String,Object> map = new HashMap<>();
        try {
            map = MapUtil.chuli(json);

        } catch (Exception e) {
            try {
                String userbh= request.getParameter("userbh");
                String urltype= request.getParameter("urltype");
                String money= request.getParameter("money");
                String paytype= request.getParameter("paytype");
                String gdtype= request.getParameter("gdtype");
                String ordernum= request.getParameter("ordernum");
                String service= request.getParameter("service");
                String resultype=request.getParameter("resultype");
                map.put("resultype",resultype);
                map.put("service",service);
                map.put("userbh",userbh);
                map.put("urltype",urltype);
                map.put("money",money);
                map.put("paytype",paytype);
                map.put("gdtype",gdtype);
                map.put("ordernum",ordernum);
            } catch (Exception e1) {
                map = JacksonUtil.parseJson(json);
            }
        }
        map.put("resultype","json");
        return ServiceResult.createResult(model,map,codePayService.createorder(map));
    }


    /**
     * 查询快递信息
     * @param code
     * @param model
     * @param request
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/kuaidi22222/{code}", method = RequestMethod.GET)
    @ResponseBody
    public Object kuaidi(@PathVariable String code,  Model model,HttpServletRequest request) throws Exception {
        Map<String,Object> map = new HashMap<>();
        map.put("resultype","string");
        return ServiceResult.createResult(model,map,codePayService.kuaidi(code));
    }


    /**
     * 推送订单状态
     * 返回:订单号
     *     金额
     *     支付时间
     */


}
