package cn.service;
import java.io.UnsupportedEncodingException;
import java.util.Map;
public interface CodePayHomeService {
    Map<String,Object> submitorder(Map<String,Object> map);
    Map<String,Object> getorderstate(Map<String,Object> map);
    int setorderstate(Map<String,Object> map) throws UnsupportedEncodingException;
}
