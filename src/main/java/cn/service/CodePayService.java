package cn.service;

import java.util.List;
import java.util.Map;

public interface CodePayService {
    public Map<String, Object> login(Map<String, Object> map) throws Exception;
    public List<Map<String, Object>> geturl(Map<String, Object> map) ;
    int updatereceivables(Map<String, Object> map) ;
    //String getmoney(Map<String, Object> map) ; 代码废弃
    Map<String,Object> createorder(Map<String, Object> map) ;
    //取今日 昨日 一周 所有  收款金额
    Map<String, Object>gettimemoney(Map<String, Object> map);
    String kuaidi(String code);
}
