package cn.service.impl;


import CodePay.Pay;
import CodePay.config.UserInfo;
import CodePay.constant.Paystate;
import cn.dao.CodePayDao;
import cn.dao.CodePayHomeDao;
import cn.service.CodePayHomeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("codePayHomeService")
public class CodePayHomeServiceImpl implements CodePayHomeService {
    //初始化码农玖付 参数:1.账号,2.Mima,3.域名id
    Pay pay;

    @Resource
    CodePayHomeDao codePayHomeDao;

    @Resource
    CodePayDao codePayDao;


   public Map<String,Object> submitorder(Map<String,Object> map){
       Map<String,Object> result=new HashMap<>();


       if(map.get("money").equals("bug？让你失望了")){
         map.put("money",100);
       }

       if(pay==null) {
           pay = new Pay("yr521521", "yr520000", 13);
       }


       List<Map<String, Object>> sourse = codePayDao.iseorder(map);
       //这里可以限制订单重复
       if (sourse.size() > 0) {
           //订单号重复
           result.put("state",1006);
       } else {
           //创建订单
           Map<String,Object> codemap= pay.createorder(map);
           if(codemap!=null&&codemap.get("state").equals(1)){
               //自己的数据库创建订单
               if(codePayHomeDao.createorder(codemap)>0){
                   result=codemap;
               }


           }else{
               result.put("state",codemap.get("state"));
           }

       }

       return result;
   }

    /**
     * 获取订单状态
     * @param map
     * @return
     */
    public Map<String, Object> getorderstate(Map<String, Object> map) {
        return codePayHomeDao.getorderstate(map);
    }

    /**
     * 码农服务器回调
     * @param map
     * @return
     */
    public int setorderstate(Map<String, Object> map) throws UnsupportedEncodingException {
        //服务器key和本地存储的key对比  对比成功当前请求就是码农官网请求来的
        if(map.get("userbh").equals(UserInfo.getUserbbh())){
            //在这里你可以获取到skordernum(订单号)，skmoney(收款金额)，time(支付时间)

            //你可以修改长时间没支付订单的状态(必须设置，因为防止恶意占用可用金额。必须设置超时时间为6分钟，和码农服务器统一)

            //你可以更新一下 你的订单信息状态根据订单号
            return codePayHomeDao.updateorder(map);
        }
        return 0;
    }
}
