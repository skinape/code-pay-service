package cn.service.impl;


import cn.dao.CodePayDao;
import cn.dao.CodePayUserDao;
import cn.service.CodePayUserService;
import cn.util.EncryptUtil;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service("codePayUserService")
public class CodePayUserServiceIpml implements CodePayUserService {
    @Resource
    CodePayUserDao codePayUserDao;

    @Resource
    CodePayDao codePayDao;

    @Override
    public Map<String, Object> getuserinfo(Map<String, Object> map) {
        //当前用户线程
        Subject subject = SecurityUtils.getSubject();
        String userbh = (String) subject.getPrincipal();
        map.put("userbh", userbh);
        return codePayUserDao.getuserinfo(map);
    }

    @Override
    public List<Map<String, Object>> geturlinfo(Map<String, Object> map) {
        //当前用户线程
        Subject subject = SecurityUtils.getSubject();
        String userbh = (String) subject.getPrincipal();
        map.put("userbh", userbh);
        return codePayDao.geturl(map);
    }

    @Override
    public List<Map<String, Object>> getreceivables(Map<String, Object> map) {
        //当前用户线程
        Subject subject = SecurityUtils.getSubject();
        String userbh = (String) subject.getPrincipal();
        map.put("userbh", userbh);
        return codePayDao.getreceivables(map);
    }

    @Override
    public Map<String, Object> gettimemoney(Map<String, Object> map) {
        //当前用户线程
        Subject subject = SecurityUtils.getSubject();
        String userbh = (String) subject.getPrincipal();
        map.put("userbh", userbh);
        return codePayDao.gettimemoney(map);
    }

    //新增域名
    public int weihuurl(Map<String, Object> map) {
        //当前用户线程
        Subject subject = SecurityUtils.getSubject();
        String userbh = (String) subject.getPrincipal();
        map.put("userbh", userbh);

        //为空 当前状态为新增域名
        if (map.get("urlid").equals("")) {
            return codePayUserDao.addurl(map);
        } else {
            //为修改域名
            return codePayUserDao.updateurl(map);
        }


    }

    //删除域名
    public int daleteurl(Map<String, Object> map) {
        //当前用户线程
        Subject subject = SecurityUtils.getSubject();
        String userbh = (String) subject.getPrincipal();
        map.put("userbh", userbh);
        return codePayUserDao.daleteurl(map);
    }

    //维护个人认证
    public int updatecertcenter(Map<String, Object> map, HttpServletRequest request) {
        String usertcardimg = "";
        //图片临时文件
        ArrayList tempimg = new ArrayList();
        //当前用户线程
        Subject subject = SecurityUtils.getSubject();
        String userbh = (String) subject.getPrincipal();
        map.put("userbh", userbh);
        //保存图片
        MultipartFile[] imgfile = (MultipartFile[]) map.get("imgfile");
        for (MultipartFile multipartFile : imgfile) {
            try {
                String path = null;// 文件路径
                String type = null;// 文件类型
                String fileName = multipartFile.getOriginalFilename();// 文件原名称
                // 判断文件类型
                type = fileName.indexOf(".") != -1 ? fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length()) : null;
                if (type != null) {// 判断文件类型是否为空
                    if ("GIF".equals(type.toUpperCase()) || "PNG".equals(type.toUpperCase()) || "JPG".equals(type.toUpperCase())) {
                        // 项目在容器中实际发布运行的根路径
                        String realPath = request.getSession().getServletContext().getRealPath("/") + "/res/images/tcardimg/";
                        // 自定义的文件名称
                        String trueFileName = String.valueOf(System.currentTimeMillis()) + "." + type.toUpperCase();
                        // 设置存放图片文件的路径
                        path = realPath + trueFileName;
                        // 转存文件到指定的路径
                        multipartFile.transferTo(new File(path));
                        usertcardimg = usertcardimg + "/res/images/tcardimg/" + trueFileName + "|";
                        //添加到缓存文件
                        tempimg.add(path);
                        for (int i = 0; i < tempimg.size(); i++) {
                            System.out.println(tempimg.get(i).toString());
                        }
                    } else {
                        System.out.println("不是我们想要的文件类型,请按要求重新上传");
                    }
                } else {
                    System.out.println("文件类型为空");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        usertcardimg = usertcardimg.substring(0, usertcardimg.length() - 1);
        map.put("usertcardimg", usertcardimg);
        int result = codePayUserDao.updatecertcenter(map);
        //维护失败，删除图片
        if (result != 1) {
            for (int i = 0; i < tempimg.size(); i++) {
                File file = new File(tempimg.get(i).toString());
                file.delete();
            }
        }
        return result;
    }

    //用户名是否存在
    public int checkLoginName(Map<String, Object> map) {
        return codePayUserDao.checkLoginName(map);
    }

    //注册用户
    public int reginfo(Map<String, Object> map) {
        try {
            if(map.containsKey("userpwd")){
                map.put("userpwd",EncryptUtil.encode(map.get("userpwd").toString()));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return codePayUserDao.reginfo(map);
    }

}
