package cn.service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

public interface CodePayUserService {
    //获取用户信息
    Map<String, Object> getuserinfo(Map<String, Object> map);
    //获取域名信息
    List<Map<String, Object>> geturlinfo(Map<String, Object> map);
    //收款信息
    List<Map<String, Object>> getreceivables(Map<String, Object> map);
    //取今日 昨日 一周 所有  收款金额
    Map<String, Object>gettimemoney(Map<String, Object> map);
    //维护域名
    int weihuurl(Map<String, Object> map);
    //删除域名
    int daleteurl(Map<String, Object> map);
    //维护个人认证
    int updatecertcenter(Map<String, Object> map,HttpServletRequest request);
    //用户名是否存在
    int checkLoginName(Map<String, Object> map);
    //注册用户
    int reginfo(Map<String, Object> map);
}
