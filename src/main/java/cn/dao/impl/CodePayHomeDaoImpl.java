package cn.dao.impl;

import cn.dao.CodePayHomeDao;
import cn.mapper.MryeMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Map;

//@Transactional
@Service("codePayHomeDao")
public class CodePayHomeDaoImpl implements CodePayHomeDao {
    @Resource(name = "mryeMapper")
    MryeMapper mryeMapper;

    /**
     * 获取订单状态
     *
     * @param map
     * @return
     */
    public Map<String, Object> getorderstate(Map<String, Object> map) {
        String sql = "SELECT  state FROM `order` WHERE ordernum=#{ordernum} LIMIT 0,1";
        map.put("sql", sql);
        return mryeMapper.searchSap(map);
    }

    /**
     * 数据库添加订单
     *
     * @param map
     * @return
     */
    public int createorder(Map<String, Object> map) {
        String sql = "INSERT INTO `order`(ordernum,sktype,skgdtype,skmoney,state)VALUES(#{ordernum},#{paytype},#{gdtype},#{money},1)";
        map.put("sql", sql);
        return mryeMapper.saveSap(map);
    }

    /**
     * 更新订单状态
     *
     * @param map
     * @return
     */
    public int updateorder(Map<String, Object> map) {
        String sql = "UPDATE `order` SET state=2,sktime=#{time} WHERE ordernum=#{skordernum}";
        map.put("sql", sql);
        return mryeMapper.updateSap(map);
    }
}
