package cn.dao.impl;

import cn.dao.CodePayUserDao;
import cn.mapper.MryeMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

@Service("codePayUserDao")
public class CodePayUserDaoImpl implements CodePayUserDao {
    @Resource(name = "mryeMapper")
    MryeMapper mryeMapper;

    /**
     * 获取用户信息
     * @param map
     * @return
     */
    public Map<String, Object> getuserinfo(Map<String, Object> map) {
        String sql="SELECT a1.userbh, a1.username,a1.usertcard,a1.usertcardimg,a1.userphone,a1.usercertcenter, a2.vipname FROM codeuser a1 LEFT JOIN vip a2 ON a1.viptype = a2.viptype WHERE a1.userbh = #{userbh}";
        map.put("sql",sql);
        return mryeMapper.searchSap(map);
    }

    //新增域名
    public int addurl(Map<String, Object> map) {
        int count=0;

        String sql="SELECT o1.vipurlnum FROM vip o1 LEFT JOIN codeuser o2 ON o2.viptype = o1.viptype WHERE o2.userbh = #{userbh}";
        map.put("sql",sql);
        //可以拥有的url数量
        int vipurlnum=(int)mryeMapper.searchSap(map).get("vipurlnum");

        sql="SELECT COUNT(1) AS num FROM url WHERE userbh=#{userbh}";
        map.put("sql",sql);
        //已拥有的url数量
        int urlnum=Integer.parseInt(mryeMapper.searchSap(map).get("num").toString()) ;

        //限制添加域名数量
        if(urlnum<vipurlnum){
            sql="INSERT INTO url(userbh,urlname,urlalias)VALUES(#{userbh},#{urlname},#{urlalias});";
            map.put("sql",sql);
            count=mryeMapper.saveSap(map);
        }else{
            count=3;
        }
        return count;
    }

    //修改域名
    public int updateurl(Map<String, Object> map) {
        String sql="UPDATE url SET urlname=#{urlname},urlalias=#{urlalias} WHERE userbh=#{userbh} AND urlid=#{urlid}";
        map.put("sql",sql);
        return mryeMapper.updateSap(map);
    }

    //删除域名
    public int daleteurl(Map<String, Object> map) {
        String sql="DELETE FROM url WHERE userbh=#{userbh} AND urlid=#{urlid}";
        map.put("sql",sql);
        return mryeMapper.deleteSap(map);
    }

    //维护个人认证
    public int updatecertcenter(Map<String, Object> map) {
        String sql="UPDATE codeuser SET username=#{username},usertcard=#{usertcard},usertcardimg=#{usertcardimg},userphone=#{userphone},usercertcenter=1 WHERE userbh=#{userbh}";
        map.put("sql",sql);
        return mryeMapper.updateSap(map);
    }

    //用户名是否存在
    public int checkLoginName(Map<String, Object> map) {
        String sql="SELECT COUNT(1) AS num FROM codeuser WHERE userbh=#{userbh}";
        map.put("sql",sql);
        return Integer.parseInt(mryeMapper.searchSap(map).get("num").toString());
    }

    //注册用户
    public int reginfo(Map<String, Object> map) {
        String sql = "SELECT  state FROM `order` WHERE ordernum=#{ordernum} AND skmoney>99 LIMIT 0,1";
        map.put("sql", sql);
        //判断订单号是否支付成功
        if(Integer.parseInt(mryeMapper.searchSap(map).get("state").toString())==2){
            sql="INSERT INTO codeuser (userbh, userpwd, userphone,userip) VALUES( #{userbh}, #{userpwd},#{phone},#{ip} )";
            map.put("sql",sql);
            return mryeMapper.saveSap(map);
        }
        return 0;

    }

}
