package cn.dao.impl;


import cn.dao.CodePayDao;
import cn.mapper.MryeMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
//@Transactional
@Service("codePayDao")
public class CodePayDaoImpl implements CodePayDao {
    @Resource(name="mryeMapper")
    MryeMapper mryeMapper;


    /**
     * 登陆接口
     * @param map
     * @return
     */
    public Map<String, Object> login(Map<String, Object> map) {
        //更新秘钥
        String sql="";
        //用户监控才更新秘钥
        if(map.containsKey("service")){
            sql="UPDATE codeuser SET userkey=#{userkey} WHERE userbh = #{userbh} AND userpwd = #{userpwd}";
            map.put("sql",sql);
            mryeMapper.updateSap(map);
        }else{
            //更新ip
            sql="UPDATE codeuser SET userip=#{ip} WHERE userbh = #{userbh} AND userpwd = #{userpwd}";
            map.put("sql",sql);
            mryeMapper.updateSap(map);
        }

     sql="SELECT a1.userbh,a1.userkey,a1.username, a2.vipname FROM codeuser a1 LEFT JOIN vip a2 ON a1.viptype = a2.viptype WHERE a1.userbh = #{userbh} AND a1.userpwd = #{userpwd}";
     map.put("sql",sql);
     return mryeMapper.searchSap(map);
    }

    /**
     * 获取收款信息
     * @param map
     * @return
     */
    public List<Map<String, Object>> getreceivables(Map<String, Object> map) {
        String sql="SELECT a1.sktype, a1.skgdtype,a1.skordernum,a2.urlname, a2.urlalias, a1.skmoney,a1.createtime, a1.skstate,a1.sktime FROM receivables a1 LEFT JOIN url a2 ON a1.skurlid = a2.urlid WHERE a1.skuserbh = #{userbh} ";
        if(map.containsKey("sktype")){
            sql=sql+" AND sktype=#{sktype} ";
        }
        sql=sql+ " ORDER BY a1.skstate,a1.createtime DESC LIMIT #{start},#{end}";
        map.put("sql",sql);
        return mryeMapper.searchSapList(map);
    }

    //更新超时支付金额
    public int updatemoney(Map<String, Object> map) {
        String sql="UPDATE receivables SET skstate=3 WHERE skstate=1 AND TIMESTAMPDIFF(MINUTE,createtime,CURRENT_TIMESTAMP)>30";
        map.put("sql",sql);
        return mryeMapper.updateSap(map);
    }

    /**
     * 获取今日 昨日 一周 所有  收款金额
     * @param map
     * @return
     */
    public Map<String, Object> gettimemoney(Map<String, Object> map) {
        String sql="SELECT FORMAT(IFNULL(b.today,0.0),2) AS today, FORMAT(IFNULL(c.yesterday,0.0),2) AS yesterday, FORMAT(IFNULL(d.weektiem,0.0),2) AS weektiem, FORMAT(IFNULL(e.alltime,0.0),2) AS alltime FROM (SELECT 1) a LEFT JOIN ( SELECT SUM(a1.skmoney) AS today FROM receivables a1 WHERE a1.skuserbh = #{userbh} AND a1.skstate = 2 AND TO_DAYS(a1.sktime) = TO_DAYS(NOW())) b ON 1 = 1 LEFT JOIN ( SELECT SUM(a1.skmoney) AS yesterday FROM receivables a1 WHERE a1.skuserbh = #{userbh} AND a1.skstate = 2 AND TO_DAYS(NOW()) - TO_DAYS(a1.sktime) <= 1 ) c ON 1 = 1 LEFT JOIN ( SELECT SUM(a1.skmoney) AS weektiem FROM receivables a1 WHERE a1.skuserbh = #{userbh} AND a1.skstate = 2 AND DATE_SUB(CURDATE(), INTERVAL 7 DAY) <= date(a1.sktime)) d ON 1 = 1 LEFT JOIN ( SELECT SUM(a1.skmoney) AS alltime FROM receivables a1 WHERE a1.skuserbh = #{userbh} AND a1.skstate = 2 ) e ON 1 = 1";
        map.put("sql",sql);
        return mryeMapper.searchSap(map);
    }

    /**
     * 获取域名信息
     * @param map
     * @return
     */
    public List<Map<String, Object>> geturl(Map<String, Object> map) {
        String sql="SELECT urlname, urlalias, urlid FROM url WHERE userbh = #{userbh}";
        if (map.containsKey("urlid")&&map.get("urlid")!=null){
            sql=sql+" AND urlid=#{urlid}";
        }
        map.put("sql",sql);
        return mryeMapper.searchSapList(map);
    }

    /**
     * 更新 收款单状态
     * @param map
     * @return
     */
    public int updatereceivables(Map<String, Object> map) {
        String sql="SELECT usercertcenter FROM codeuser WHERE userbh=#{userbh}";
        map.put("sql",sql);
        int renzheng=(int)mryeMapper.searchSap(map).get("usercertcenter");
        //是否认证
        if(renzheng==2) {
             sql = "UPDATE receivables SET skstate=2,sktime=current_timestamp() WHERE skuserbh=#{userbh} AND sktype=#{sktype}  AND skmoney=#{skmoney} AND skstate=1";
            map.put("sql", sql);
            return mryeMapper.updateSap(map);
        }
        return 0;
    }

    /**
     * 获取订单信息
     * @param map
     * @return
     */
    public Map<String, Object> getorder(Map<String, Object> map) {
        String sql="SELECT T3.userbh,T3.userkey AS manongkey,T1.skordernum,T1.sktype,T1.skgdtype,T1.skmoney,T2.urlname,current_timestamp() AS time FROM receivables T1 LEFT JOIN url T2 ON T1.skurlid=T2.urlid LEFT JOIN codeuser T3 ON T1.skuserbh=T3.userbh  WHERE T1.skuserbh=#{userbh} AND T1.sktype=#{sktype} AND T1.skmoney=#{skmoney} AND T1.skstate=1";
        map.put("sql",sql);
        return mryeMapper.searchSap(map);
    }

    /**
     * 获取可用金额
     * @param map
     * @return
     */
    public List<Map<String, Object>> getmoney(Map<String, Object> map) {
        String sql="SELECT usercertcenter FROM codeuser WHERE userbh=#{userbh}";
        map.put("sql",sql);
        int renzheng=(int)mryeMapper.searchSap(map).get("usercertcenter");
        //是否认证
        if(renzheng==2) {
             sql = "SELECT if(LENGTH(a1.skmoney)>3,CONCAT('0.', RIGHT(a1.skmoney, 2)),CONCAT('0', RIGHT(a1.skmoney, 2)))AS money FROM receivables a1 LEFT JOIN url a2 ON a1.skurlid = a2.urlid WHERE a1.skuserbh = #{userbh}  AND a1.sktype=#{paytype} AND a1.skgdtype=#{gdtype} AND a1.skmoney LIKE CONCAT(#{money},'.%') AND a1.skstate=1 ";
            map.put("sql", sql);
            return mryeMapper.searchSapList(map);
        }
        return null;
    }

    /**
     * 创建订单
     * @param map
     * @return
     */
    public int createorder(Map<String, Object> map) {
        String sql="SELECT usercertcenter FROM codeuser WHERE userbh=#{userbh}";
        map.put("sql",sql);
       int renzheng=(int)mryeMapper.searchSap(map).get("usercertcenter");
       //是否认证
        if(renzheng==2){
            sql="INSERT INTO receivables ( skuserbh, skgdtype, sktype, skordernum, skurlid, skmoney, skstate ) VALUES ( #{userbh},#{gdtype},#{paytype},#{ordernum},#{urltype},#{money},1);";
            map.put("sql",sql);
            return Integer.valueOf(mryeMapper.saveSap(map)) ;
        }else{
            return 1007;
        }

    }

    /**
     * 是否存在当前订单号
     * @param map
     * @return
     */
    public List<Map<String,Object>> iseorder(Map<String, Object> map) {
        String sql="SELECT skordernum FROM receivables WHERE skuserbh=#{userbh} AND skurlid=#{urltype} AND skordernum=#{ordernum}";
        map.put("sql",sql);
        return mryeMapper.searchSapList(map);
    }
}
