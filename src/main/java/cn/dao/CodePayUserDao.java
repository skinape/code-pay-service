package cn.dao;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public interface CodePayUserDao {
    //获取个人信息
    Map<String, Object> getuserinfo(Map<String, Object> map);
    //新增域名
    int addurl(Map<String, Object> map);
    //修改域名
    int updateurl(Map<String, Object> map);
    //删除域名
    int daleteurl(Map<String, Object> map);
    //维护个人认证
    int updatecertcenter(Map<String, Object> map);
    //用户名是否存在
    int checkLoginName(Map<String, Object> map);
    //注册用户
    int reginfo(Map<String, Object> map);
}
