package cn.dao;

import java.util.Map;

public interface CodePayHomeDao {
    Map<String,Object> getorderstate(Map<String,Object> map);
    int createorder(Map<String,Object> map);
    int updateorder(Map<String,Object> map);
}
