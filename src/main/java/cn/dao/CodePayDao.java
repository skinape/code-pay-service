package cn.dao;

import java.util.List;
import java.util.Map;

public interface CodePayDao {
    //登陆接口
    Map<String, Object> login(Map<String, Object> map);
    //获取收款信息
    List<Map<String, Object>> getreceivables(Map<String, Object> map) ;
    //更新超时支付金额
    int updatemoney(Map<String, Object> map) ;
    //获取域名信息
    List<Map<String, Object>> geturl(Map<String, Object> map) ;
    int updatereceivables(Map<String, Object> map) ;
    Map<String, Object> getorder(Map<String, Object> map) ;
    List<Map<String, Object>> getmoney(Map<String, Object> map) ;
    int createorder(Map<String, Object> map) ;
    //是否存在当前订单号
    List<Map<String,Object>> iseorder(Map<String, Object> map) ;
    //取今日 昨日 一周 所有  收款金额
    Map<String, Object>gettimemoney(Map<String, Object> map);
}
