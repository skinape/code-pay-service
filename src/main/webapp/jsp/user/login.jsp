<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>登录 | 码农玖付</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Coderthemes" name="author" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="./favicon.ico">

    <!-- App css -->
    <!-- build:css -->
    <link href="/res/css/app.min.css" rel="stylesheet" type="text/css" />
    <!-- endbuild -->

</head>

<body class="authentication-bg">

<div class="account-pages mt-5 mb-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-5">
                <div class="card">

                    <!-- Logo -->
                    <div class="card-header pt-4 pb-4 text-center bg-primary">
                        <a href="">
                            <span><img src="http://www.akpay.cn/assets/img/logo.png" alt="" height="40"></span>
                        </a>
                    </div>

                    <div class="card-body p-4">

                        <div class="text-center w-75 m-auto">
                            <h4 class="text-dark-50 text-center mt-0 font-weight-bold">Sign In</h4>
                        </div>

                        <form class="form" method="post" action="/login.json">

                            <div class="form-group mb-3">
                                <label for="emailaddress">您的账号</label>
                                <input class="form-control" type="text" name="userbh" value="" placeholder="用户编号" required>
                            </div>

                            <div class="form-group mb-3">
                                <label for="password">您的密码</label>
                                <input class="form-control" type="password" name="userpwd" value="" placeholder="用户密码" required>
                                <!--  <a href="findpwd.php" class="text-muted float-right"><small>忘记账户?</small></a> -->
                            </div>
                                <input name="path" value="user/transfer" hidden="hidden">
                                <input name="login" value="1" hidden="hidden">
                                <input name="ip"  hidden="hidden">
                            <div class="form-group mb-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="checkbox-signin" checked>
                                    <label class="custom-control-label" for="checkbox-signin">Remember me</label>
                                </div>
                            </div>

                            <div class="form-group mb-0 text-center">
                                <button class="btn btn-primary" type="submit"> Log In </button>
                                <%--<br/><br/>--%>
                                <%--<p class="link">其他方式登陆：<a href="connect.php">QQ</a> <a href="oauth.php">支付宝</a></p>--%>
                            </div>

                        </form>
                    </div> <!-- end card-body -->
                </div>
                <!-- end card -->

                <div class="row mt-3">
                    <div class="col-12 text-center">
                        <p class="text-muted"> 没有账户？<a href="reg.html" class="text-dark ml-1"><b>申请商户</b></a></p>
                    </div> <!-- end col -->
                </div>
                <!-- end row -->

            </div> <!-- end col -->
        </div>
        <!-- end row -->
    </div>
    <!-- end container -->
</div>
<!-- end page -->

<footer class="footer footer-alt">
    2019 © 码农玖付        </footer>
<script type="text/javascript" src="<%=request.getContextPath()%>/res/lib/jquery/1.9.1/jquery.min.js"></script>
<script src="http://pv.sohu.com/cityjson?ie=utf-8"></script>
<script>
    $("[name=ip]").val(returnCitySN["cip"]+" --"+returnCitySN["cname"]);
</script>
</body>
</html>