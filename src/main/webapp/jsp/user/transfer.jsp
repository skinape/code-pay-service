<%@ page import="java.util.Map" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
</head>
<body>
<%
        Map<String,Object> errorInfo = (Map<String,Object>)request.getAttribute("result");
        if(errorInfo.containsKey("state")) {
%>
<script type="text/javascript" language="javascript">
        alert("<%=errorInfo.get("message")%>");
        window.location='user/login.html' ; // 登陆失败跳转到用户登陆界面
</script>
<%
        }else{
%>
<script type="text/javascript" language="javascript">
        window.location='user/index.html' ; // 登陆成功跳转到用户主界面
</script>
<%
        }
%>
</body>
</html>
