﻿<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <link rel="Bookmark" href="favicon.ico">
    <link rel="Shortcut Icon" href="favicon.ico"/>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="<%=request.getContextPath()%>/res/lib/html5.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/res/lib/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/res/h-ui/css/H-ui.min.css"/>
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/res/h-ui.admin/css/H-ui.admin.css"/>
    <link rel="stylesheet" type="text/css"
          href="<%=request.getContextPath()%>/res/lib/Hui-iconfont/1.0.8/iconfont.css"/>
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/res/h-ui.admin/skin/default/skin.css"
          id="skin"/>
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/res/h-ui.admin/css/style.css"/>
    <!--[if IE 6]>
    <script type="text/javascript" src="http://lib.h-ui.net/DD_belatedPNG_0.0.8a-min.js"></script>
    <script>DD_belatedPNG.fix('*');</script>
    <![endif]-->
    <!--/meta 作为公共模版分离出去-->

    <title>认证中心-个人后台</title>
    <meta name="keywords" content="H-ui.admin v3.0,H-ui网站后台模版,后台模版下载,后台管理系统模版,HTML后台模版下载">
    <meta name="description" content="H-ui.admin v3.0，是一款由国人开发的轻量级扁平化网站后台模板，完全免费开源的网站后台管理系统模版，适合中小型CMS后台系统。">
</head>
<body>
<!--_header 作为公共模版分离出去-->
<header class="navbar-wrapper">
    <div class="navbar navbar-fixed-top">
        <div class="container-fluid cl"><a class="logo navbar-logo f-l mr-10 hidden-xs" href="/">码农玖付-个人后台</a>
            <a class="logo navbar-logo-m f-l mr-10 visible-xs" href="/">CodePay</a>
            <a aria-hidden="false" class="nav-toggle Hui-iconfont visible-xs" href="javascript:;">&#xe667;</a>
            <nav id="Hui-userbar" class="nav navbar-nav navbar-userbar hidden-xs">
                <ul class="cl">
                    <li><a href="http://codepay.baldhome.com/CodePay.apk" style="color: red;">安卓APP下载(必下载)</a></li>
                    <li>${result.userinfo.vipname}</li>
                    <li class="dropDown dropDown_hover"><a href="#" class="dropDown_A">${result.userinfo.username} <i
                            class="Hui-iconfont">&#xe6d5;</i></a>
                        <ul class="dropDown-menu menu radius box-shadow">
                            <li><a href="javascript:;" onClick="myselfinfo()">个人信息</a></li>
                            <li><a href="#">退出</a></li>
                        </ul>
                    </li>
                    <li id="Hui-msg"><a href="#" title="消息"><span class="badge badge-danger">1</span><i
                            class="Hui-iconfont" style="font-size:18px">&#xe68a;</i></a></li>
                    <li id="Hui-skin" class="dropDown right dropDown_hover"><a href="javascript:;" class="dropDown_A"
                                                                               title="换肤"><i class="Hui-iconfont"
                                                                                             style="font-size:18px">&#xe62a;</i></a>
                        <ul class="dropDown-menu menu radius box-shadow">
                            <li><a href="javascript:;" data-val="default" title="默认（黑色）">默认（黑色）</a></li>
                            <li><a href="javascript:;" data-val="blue" title="蓝色">蓝色</a></li>
                            <li><a href="javascript:;" data-val="green" title="绿色">绿色</a></li>
                            <li><a href="javascript:;" data-val="red" title="红色">红色</a></li>
                            <li><a href="javascript:;" data-val="yellow" title="黄色">黄色</a></li>
                            <li><a href="javascript:;" data-val="orange" title="橙色">橙色</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</header>
<!--/_header 作为公共模版分离出去-->

<!--_menu 作为公共模版分离出去-->
<aside class="Hui-aside" style="margin-top: 40px;">

    <div class="menu_dropdown bk_2">
        <dl id="menu-member">
            <dt><i class="Hui-iconfont">&#xe60d;</i> 个人中心<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
            <dd>
                <ul>
                    <li><a href="certcenter.html" title="个人认证">个人认证</a></li>
                </ul>
            </dd>
        </dl>
        <dl id="menu-tongji">
            <dt><i class="Hui-iconfont">&#xe61a;</i> 盈利统计<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
            <dd>
                <ul>
                    <li><a href="profit.html?sktype=1&start=0" title="支付宝盈利统计">支付宝盈利统计</a></li>
                    <li><a href="profit.html?sktype=2&start=0" title="微信盈利统计">微信盈利统计</a></li>
                </ul>
            </dd>
        </dl>
        <dl id="menu-system">
            <dt><i class="Hui-iconfont">&#xe62e;</i> 域名管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
            <dd>
                <ul>
                    <li><a href="urls.html" title="域名管理">域名管理</a></li>
                </ul>
            </dd>
        </dl>
    </div>
</aside>
<div class="dislpayArrow hidden-xs"><a class="pngfix" href="javascript:void(0);" onClick="displaynavbar(this)"></a>
</div>
<!--/_menu 作为公共模版分离出去-->

<section class="Hui-article-box">
    <nav class="breadcrumb"><i class="Hui-iconfont"></i> <a href="/user/index.html" class="maincolor">首页</a>
        <span class="c-999 en">&gt;</span>
        <span class="c-666">认证中心</span>
        <a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px"
           href="javascript:location.replace(location.href);" title="刷新"><i class="Hui-iconfont">&#xe68f;</i></a></nav>
    <div class="Hui-article" style="margin-top: 40px;">
        <article class="cl pd-20 dataTables_wrapper">
            <c:choose>
                <c:when test="${result.userinfo.usercertcenter==0}">
                    <form action="updatecertcenter.json" method="post" class="form form-horizontal" id="form-article-add"
                          enctype="multipart/form-data">
                        <div class="row cl">
                            <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>姓名：</label>
                            <div class="formControls col-xs-8 col-sm-9">
                                <input type="text" class="input-text" value="${result.userinfo.username}" placeholder=""
                                       name="name">
                            </div>
                        </div>
                        <div class="row cl">
                            <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>省份证：</label>
                            <div class="formControls col-xs-8 col-sm-9">
                                <input type="text" class="input-text" value="${result.userinfo.usertcard}"
                                       placeholder="" name="tcard">
                            </div>
                        </div>
                        <div class="row cl">
                            <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>省份证照片(多选)：</label>
                            <div class="formControls col-xs-8 col-sm-9">
                                <div style="padding: 20px">
                                    <input onchange="FirstImg()" name="imgfile" style="opacity:0;position:absolute;" type="file" id="FirstfileImg" multiple="">
                                    <div>
                                        <span style="color: #04c;"> 点击这里上传图片&nbsp;<span style="color: red;">(必须一次性选择俩张图片)</span></span>
                                    </div>
                                </div>
                                <fieldset style="width:500px;">
                                    <div style="position: relative;" id="cc">
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div class="row cl">
                            <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>电话：</label>
                            <div class="formControls col-xs-8 col-sm-9">
                                <input type="text" class="input-text" value="${result.userinfo.userphone}"
                                       placeholder="" name="phone">
                            </div>
                        </div>
                        <div class="row cl">
                            <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>认证状态：</label>
                            <div class="formControls col-xs-8 col-sm-9">
                                <p><span style="color: red;">【未认证】</span></p>
                            </div>
                        </div>
                        <div class="row cl">
                            <div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-2">
                                <button class="btn btn-primary radius" type="submit" id="submit_btn"><i
                                        class="Hui-iconfont"></i> 提交
                                </button>
                                <button onclick="layer_close();" class="btn btn-default radius" type="button">&nbsp;&nbsp;取消&nbsp;&nbsp;</button>
                            </div>
                        </div>
                    </form>
                </c:when>
                <c:otherwise>
                    <div class="form form-horizontal"
                    >
                        <div class="row cl">
                            <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>姓名：</label>
                            <div class="formControls col-xs-8 col-sm-9">
                                <p>${result.userinfo.username}</p>
                            </div>
                        </div>
                        <div class="row cl">
                            <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>省份证：</label>
                            <div class="formControls col-xs-8 col-sm-9">
                                <p>${result.userinfo.usertcard}</p>
                            </div>
                        </div>
                        <div class="row cl">
                            <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>省份证照片：</label>
                            <div class="formControls col-xs-8 col-sm-9">
                                    <%--一个商品图片字段  有多个商品--%>
                                <c:set value="${ fn:split(result.userinfo.usertcardimg, '|') }" var="TPList"/>
                                <p style="width:400px;"><img style="width: 50%" src="${TPList[0]}"/><img
                                        style="width: 50%" src="${TPList[1]}"/></p>
                            </div>
                        </div>
                        <div class="row cl">
                            <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>电话：</label>
                            <div class="formControls col-xs-8 col-sm-9">
                                <p>${result.userinfo.userphone}</p>
                            </div>
                        </div>
                        <div class="row cl">
                            <label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>认证状态：</label>
                            <div class="formControls col-xs-8 col-sm-9">
                                <c:choose>
                                    <c:when test="${result.userinfo.usercertcenter==1}">
                                        <p><span style="color: green;">【待审核】联系QQ:47104041。进行人工审核</span></p>
                                    </c:when>
                                    <c:otherwise>
                                        <p><span style="color: green;">【已认证】</span></p>
                                    </c:otherwise>

                                </c:choose>
                            </div>
                        </div>
                    </div>
                </c:otherwise>
            </c:choose>


        </article>
        <footer class="footer">
            <p>
                感谢jQuery、layer、laypage、Validform、UEditor、My97DatePicker、iconfont、Datatables、WebUploaded、icheck、highcharts、bootstrap-Switch<br>
                Copyright &copy;2015 H-ui.admin v3.0 All Rights Reserved.<br> 本后台系统由<a
                    href="http://www.h-ui.net/H-ui.admin.shtml" target="_blank"
                    title="H-ui.admin后端框架">H-ui.admin后端框架</a>提供后端技术支持</p>
        </footer>

    </div>
</section>

<!--_footer 作为公共模版分离出去-->
<script type="text/javascript" src="<%=request.getContextPath()%>/res/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/res/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/res/h-ui/js/H-ui.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/res/h-ui.admin/js/H-ui.admin.page.js"></script>
<!--/_footer /作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript">
    var fileArr = [];
    jQuery.DuoImgsYulan = function (file, id) {
        for (var i = 0; i < 3; i++) {
            if (!/image\/\w+/.test(file[i].type)) {
                alert("请选择图片文件");
                return false;
            }
            if (file[i].size > 2048 * 1024) {
                alert("图片不能大于2MB");
                continue;
            }
            var img;
            var reader = new FileReader();
            reader.onloadstart = function (e) {
                console.log("开始读取....");
            }
            reader.onprogress = function (e) {
                console.log("正在读取中....");
            }
            reader.onabort = function (e) {
                console.log("中断读取....");
            }
            reader.onerror = function (e) {
                console.log("读取异常....");
            }
            reader.onload = function (e) {
                console.log("成功读取....");
                var div = document.createElement("div"); //外层 div
                div.setAttribute("style", "position:relative;width:inherit;height:inherit;float:left;z-index:2;width:150px;margin-left:8px;margin-right:8px;");
                var del = document.createElement("div"); //删除按钮div
                del.setAttribute("style", "position: absolute; bottom: 4px; right: 0px; z-index: 99; width: 30px; height:30px;border-radius:50%;")
                var delicon = document.createElement("img");
                delicon.setAttribute("src", "/res/images/cha.png");
                delicon.setAttribute("title", "删除");
                delicon.setAttribute("style", "cursor:pointer;width: 30px; height:30px");
                del.onclick = function () {
                    this.parentNode.parentNode.removeChild(this.parentElement);
                    ClearfirtsImg();
                };
                del.appendChild(delicon);
                div.appendChild(del);
                var imgs = document.createElement("img"); //上传的图片
                imgs.setAttribute("name", "loadimgs");
                imgs.setAttribute("src", e.target.result);
                imgs.setAttribute("width", 150);
                if (document.getElementById(id).childNodes.length > 2) {
                    document.getElementById(id).removeChild(document.getElementById(id).firstChild);
                }
                div.appendChild(imgs)
                document.getElementById(id).appendChild(div);
            }
            reader.readAsDataURL(file[i]);
            fileArr.push(file[i]);
        }
    }

    function FirstImg() {
        $.DuoImgsYulan(document.getElementById("FirstfileImg").files, "cc");
    }

    function ClearfirtsImg() {
        var file = $("#FirstfileImg")
        file.after(file.clone().val(""));
        file.remove();
    }
</script>
<!--/请在上方写此页面业务相关的脚本-->

</body>
</html>