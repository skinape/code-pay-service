<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!--_meta 作为公共模版分离出去-->
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<meta name="renderer" content="webkit|ie-comp|ie-stand">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<link rel="Bookmark" href="favicon.ico" >
	<link rel="Shortcut Icon" href="favicon.ico" />
	<!--[if lt IE 9]>
	<script type="text/javascript" src="<%=request.getContextPath()%>/res/lib/html5.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/res/lib/respond.min.js"></script>
	<![endif]-->
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/res/h-ui/css/H-ui.min.css" />
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/res/h-ui.admin/css/H-ui.admin.css" />
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/res/lib/Hui-iconfont/1.0.8/iconfont.css" />
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/res/h-ui.admin/skin/default/skin.css" id="skin" />
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/res/h-ui.admin/css/style.css" />
	<!--[if IE 6]>
	<script type="text/javascript" src="http://lib.h-ui.net/DD_belatedPNG_0.0.8a-min.js" ></script>
	<script>DD_belatedPNG.fix('*');</script><![endif]-->
	<!--/meta 作为公共模版分离出去-->

	<link href="<%=request.getContextPath()%>/res/lib/webuploader/0.1.5/webuploader.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="page-container">
	<form action="" method="post" class="form form-horizontal" id="form-article-add">
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>回调域名：</label>
			<div class="formControls col-xs-8 col-sm-9">
				<input type="text" class="input-text" value="${result.urlinfo.urlname}" placeholder=""  name="urlname">
				<input type="text" class="input-text" value="${result.urlinfo.urlid}" hidden="hidden" placeholder=""  name="urlid">
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-2"><span class="c-red">*</span>域名别名：</label>
			<div class="formControls col-xs-8 col-sm-9">
				<input type="text" class="input-text" value="${result.urlinfo.urlalias}" placeholder=""  name="urlalias">
			</div>
		</div>
                <div class="row cl">
                    <div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-2">
                    <button   class="btn btn-primary radius" type="button" id="submit_btn"><i class="Hui-iconfont">&#xe632;</i> 提交</button>
                <button onClick="layer_close();" class="btn btn-default radius" type="button">&nbsp;&nbsp;取消&nbsp;&nbsp;</button>
                </div>
                </div>
                </form>
                </div>

                <!--_footer 作为公共模版分离出去-->
                <script type="text/javascript" src="<%=request.getContextPath()%>/res/lib/jquery/1.9.1/jquery.min.js"></script>
				<script type="text/javascript" src="<%=request.getContextPath()%>/res/lib/layer/2.4/layer.js"></script>
				<script type="text/javascript" src="<%=request.getContextPath()%>/res/h-ui/js/H-ui.js"></script>
				<script type="text/javascript" src="<%=request.getContextPath()%>/res/h-ui.admin/js/H-ui.admin.page.js"></script>
				<!--/_footer /作为公共模版分离出去-->

				<!--请在下方写此页面业务相关的脚本-->
				<script type="text/javascript" src="<%=request.getContextPath()%>/res/lib/jquery.validation/1.14.0/jquery.validate.js"></script>
				<script type="text/javascript" src="<%=request.getContextPath()%>/res/lib/jquery.validation/1.14.0/validate-methods.js"></script>
				<script type="text/javascript" src="<%=request.getContextPath()%>/res/lib/jquery.validation/1.14.0/messages_zh.js"></script>
				<script type="text/javascript" src="<%=request.getContextPath()%>/res/lib/webuploader/0.1.5/webuploader.min.js"></script>
				<script type="text/javascript" src="<%=request.getContextPath()%>/res/lib/ueditor/1.4.3/ueditor.config.js"></script>
				<script type="text/javascript" src="<%=request.getContextPath()%>/res/lib/ueditor/1.4.3/ueditor.all.min.js"> </script>
				<script type="text/javascript" src="<%=request.getContextPath()%>/res/lib/ueditor/1.4.3/lang/zh-cn/zh-cn.js"></script>
				<script type="text/javascript">
                    $(function(){
                        //提交
                        $("#submit_btn").click(function () {
                            var data=JSON.stringify($('#form-article-add').serializeObject());
                            $.ajax({
                                url:"weihuurl.json",
                                type:"POST",
                                datatype:"JSON",
                                data:data,
                                contentType: 'application/json',
                                success:function (result) {
                                    if(result==1){
                                        $("#form-article-add").find('input[type=text],select,input[type=hidden]').each(function() {
                                            $(this).val('');
                                        });
                                        layer.msg('操作成功!', {icon: 6, time: 1000});
                                        parent.location.href='urls.html';
                                        //var index = parent.layer.getFrameIndex(window.name);
                                        //parent.$('.btn-refresh').click();
                                        //parent.layer.close(index);


                                    }else if(result==3){
                                        layer.msg('添加域名已上限，请联系管理员！', {icon: 1, time: 1000});
                                    }else {
                                        layer.msg('操作失败!', {icon: 1, time: 1000});
                                    }
                                },

                            });
                        });
                    });

				</script>
</body>
</html>