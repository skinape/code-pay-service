<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>申请商户 | Ak云支付</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description"/>
    <meta content="Coderthemes" name="author"/>
    <!-- App favicon -->
    <link rel="shortcut icon" href="images/favicon.ico">

    <!-- App css -->
    <link href="/res/css/app.min.css" rel="stylesheet" type="text/css"/>

</head>

<body class="authentication-bg">

<div class="account-pages mt-5 mb-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-5">
                <div class="card">
                    <!-- Logo-->
                    <div class="card-header pt-4 pb-4 text-center bg-primary">
                        <a href="index.html">
                            <span><img src="http://www.akpay.cn/assets/img/logo.png" alt="" height="40"></span>
                        </a>
                    </div>

                    <div class="card-body p-4">
                        <form name=codepayfrom action="" method="post">

                            <div class="text-center w-75 m-auto">
                                <h4 class="text-dark-50 text-center mt-0 font-weight-bold">注册商户</h4>
                                <p class="text-muted mb-4">
                                <p>申请费用：<b>10</b>￥</p></p>                                </div>


                            <div class="form-group">
                                <label for="emailaddress">账号</label>
                                <input type="text" name="userbh" onblur="getNameStatus(this.value);" placeholder="账号"
                                       class="form-control" required>
                                <p id="userName_tip" style="margin:0px;font-size: 12px; color: #ff0000;"></p>
                            </div>

                            <div class="form-group">
                                <label for="password">密码</label>
                                <input type="text" name="userpwd" placeholder="密码" class="form-control" required>
                            </div>


                            <div class="form-group">
                                <label for="password">手机号码</label>
                                <input type="text" name="phone" placeholder="手机号码" class="form-control" required>
                                <input hidden="hidden" name="ip">
                                <input hidden="hidden" name="ordernum">
                                <input hidden="hidden" name="gdtype" value="1">
                                <input hidden="hidden" name="money" value="bug？让你失望了">
                            </div>
                            <div class="input-group" >
                                <span class="input-group-addon" style="color: red;"> 支付方式(必选):</span>
                                <input type="radio" checked="checked" name="paytype" value="1"/>支付宝
                                <input type="radio" name="paytype" value="2"/>微信
                            </div>


                            <div class="form-group" style=" display: none">
                                <label for="password">验证码</label>
                                <div class="input-group">
                                    <input type="text" name="code" placeholder="短信验证码" class="form-control" required>
                                    <div class="input-group-append">
                                        <button class="btn btn-dark" type="button" id="sendsms">获取验证码</button>
                                    </div>
                                </div>
                            </div>
                            <div class="input-group" id="erweima">

                            </div>
                            <div class="input-group">
                                <div class="form-group mb-0 text-center">
                                    <button class="btn btn-primary submit" type="button">注册商户</button>
                                </div>
                            </div> <!-- end card-body -->
                        </form>
                    </div>
                    <!-- end card -->


                    <div class="col-12 text-center">
                        <p class="text-muted">已有账户? <a href="login.html" class="text-dark ml-1"><b>登陆商户</b></a></p>
                    </div> <!-- end col-->

                    <!-- end row -->

                </div> <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
    <!-- end page -->

    <footer class="footer footer-alt">
        2019 © 码农玖付
    </footer>

    <!-- App js -->
    <script type="text/javascript" src="<%=request.getContextPath()%>/res/lib/jquery/1.9.1/jquery.min.js"></script>
    <script src="http://pv.sohu.com/cityjson?ie=utf-8"></script>
    <script>
        var isExist = 0;

        //检测用户名是否存在
        function checkLoginName(loginName) {
            $.ajax({
                url: "checkLoginName.json",
                data: JSON.stringify($('[name=userbh]').serializeObject()),
                type: "post",
                datatype: "JSON",
                contentType: 'application/json',
                async: false,
                success: function (data) {
                    isExist = data;
                },
                error: function () {
                    console.log("系统错误，请与管理员联系！");
                }
            });
            return isExist;


        }

        function ip() {
            $("[name=ip]").val(returnCitySN["cip"] + " --" + returnCitySN["cname"]);
        }

        function getNameStatus(loginName) {
            //是用户名
            var isExist = checkLoginName(loginName)
            if (isExist == 1) {
                $("#userName_tip").text("该用户名已存在，请重新输入用户名！").css("color", "#ff0000");
            } else {
                $("#userName_tip").html("").removeClass("register-tip");
                if (loginName!=""&&loginName.indexOf("/") < 0)
                    $("#userName_tip").text("该用户名可以注册！").css("color", "#00ff00");
                ip();
            }
        }

        //状态
        var state;
        //订单号
        var ordernum;
        var timeid;

        $(function () {
            //赋订单号值
            $("[name=ordernum]").val(randomNumber());

            //提交
            $(".submit").click(function () {
                if(isExist == 1||$('[name=userbh]').val()==''){
                    alert("请填写用户编号！");
                    return false;
                }else if($('[name=userpwd]').val()==''){
                    alert("请填写用户密码！");
                    return false;
                }else if($('[name=phone]').val()==''){
                    alert("请填写用户手机号！");
                    return false;
                }else{
                    var data = JSON.stringify($('[name=codepayfrom]').serializeObject());
                    $.ajax({
                        url: "/submitorder.json",
                        type: "POST",
                        datatype: "JSON",
                        data: data,
                        contentType: 'application/json',
                        success: function (result) {
                            state = result.result.state;
                            ordernum = result.result.ordernum;
                            if (state == 1) {
                                //1为支付宝
                                if (result.result.paytype == 1) {
                                    $("#erweima").append("<img  style='width: 200px;height: 200px;' src='/res/images/zhifubao.jpg'><p style='color: red;'>你需要支付:" + result.result.money + "元</p>");
                                } else {
                                    $("#erweima").append("<img  style='width: 200px;height: 200px;' src='/res/images/weixin.jpg'><p style='color: red;'>你需要支付:" + result.result.money + "元</p>");
                                }
                                timeid = setInterval("showAuto()", 1000);
                            } else if (state == 1006) {
                                alert("订单号重复");
                            } else if (state == 1007) {
                                alert("商户账号未认证,请联系管理员");
                            }


                        },

                    });
                }


            });


        });

        //监视是否充值成功
        function showAuto() {
            if (state != undefined && state == 1) {
                $.ajax({
                    url: "/getorderstate.json",
                    type: "POST",
                    datatype: "JSON",
                    data: "{\"ordernum\":\"" + ordernum + "\"}",
                    contentType: 'application/json',
                    success: function (result) {
                        if (result.result.state == 2) {
                            clearInterval(timeid);
                            $("#erweima").remove();
                            //提交注册信息
                            sbmitreg();
                        }
                    },

                });
            }

        }

        //提交注册
        function sbmitreg() {
            var data = JSON.stringify($('[name=codepayfrom]').serializeObject());
            $.ajax({
                url: "reg.json",
                type: "POST",
                datatype: "JSON",
                data: data,
                contentType: 'application/json',
                success: function (result) {
                    if (result == 1) {
                        alert("注册成功！")
                        location.href = 'login.html';
                    } else {
                        alert("注册失败！")
                    }
                },

            });
        }

        //   根据当前时间和随机数生成流水号
        function randomNumber() {
            var now = new Date();
            var year = now.getFullYear();       //年
            var month = now.getMonth() + 1;     //月
            var day = now.getDate();            //日
            var hh = now.getHours();            //时
            var mm = now.getMinutes();          //分
            var ss = now.getSeconds();           //秒
            var clock = year + "";
            if (month < 10)
                clock += "0";

            clock += month + "";

            if (day < 10)
                clock += "0";

            clock += day + "";

            if (hh < 10)
                clock += "0";

            clock += hh + "";
            if (mm < 10) clock += '0';
            clock += mm + "";

            if (ss < 10) clock += '0';
            clock += ss;

            var outTradeNo = "";  //订单号
            for (var i = 0; i < 3; i++) //6位随机数，用以加在时间戳后面。
            {
                outTradeNo += Math.floor(Math.random() * 10);
            }
            clock += outTradeNo;
            return (clock);
        }

        function func() {
            return false;
        }


    </script>
</body>
</html>
