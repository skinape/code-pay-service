﻿<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <link rel="Bookmark" href="favicon.ico">
    <link rel="Shortcut Icon" href="favicon.ico"/>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="<%=request.getContextPath()%>/res/lib/html5.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/res/lib/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/res/h-ui/css/H-ui.min.css"/>
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/res/h-ui.admin/css/H-ui.admin.css"/>
    <link rel="stylesheet" type="text/css"
          href="<%=request.getContextPath()%>/res/lib/Hui-iconfont/1.0.8/iconfont.css"/>
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/res/h-ui.admin/skin/default/skin.css"
          id="skin"/>
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/res/h-ui.admin/css/style.css"/>
    <!--[if IE 6]>
    <script type="text/javascript" src="http://lib.h-ui.net/DD_belatedPNG_0.0.8a-min.js"></script>
    <script>DD_belatedPNG.fix('*');</script>
    <![endif]-->
    <!--/meta 作为公共模版分离出去-->

    <title>码农玖付-个人后台</title>
    <meta name="keywords" content="H-ui.admin v3.0,H-ui网站后台模版,后台模版下载,后台管理系统模版,HTML后台模版下载">
    <meta name="description" content="H-ui.admin v3.0，是一款由国人开发的轻量级扁平化网站后台模板，完全免费开源的网站后台管理系统模版，适合中小型CMS后台系统。">
</head>
<body>
<!--_header 作为公共模版分离出去-->
<header class="navbar-wrapper">
    <div class="navbar navbar-fixed-top">
        <div class="container-fluid cl"><a class="logo navbar-logo f-l mr-10 hidden-xs" href="/">码农玖付-个人后台</a>
            <a class="logo navbar-logo-m f-l mr-10 visible-xs" href="/">CodePay</a>
            <a aria-hidden="false" class="nav-toggle Hui-iconfont visible-xs" href="javascript:;">&#xe667;</a>
            <nav id="Hui-userbar" class="nav navbar-nav navbar-userbar hidden-xs">
                <ul class="cl">
                    <li><a href="http://codepay.baldhome.com/CodePay.apk" style="color: red;">安卓APP下载(必下载)</a></li>
                    <li>${result.userinfo.vipname}</li>
                    <li class="dropDown dropDown_hover"><a href="#" class="dropDown_A">${result.userinfo.username} <i
                            class="Hui-iconfont">&#xe6d5;</i></a>
                        <ul class="dropDown-menu menu radius box-shadow">
                            <li><a href="javascript:;" onClick="myselfinfo()">个人信息</a></li>
                            <li><a href="#">退出</a></li>
                        </ul>
                    </li>
                    <li id="Hui-msg"><a href="#" title="消息"><span class="badge badge-danger">1</span><i
                            class="Hui-iconfont" style="font-size:18px">&#xe68a;</i></a></li>
                    <li id="Hui-skin" class="dropDown right dropDown_hover"><a href="javascript:;" class="dropDown_A"
                                                                               title="换肤"><i class="Hui-iconfont"
                                                                                             style="font-size:18px">&#xe62a;</i></a>
                        <ul class="dropDown-menu menu radius box-shadow">
                            <li><a href="javascript:;" data-val="default" title="默认（黑色）">默认（黑色）</a></li>
                            <li><a href="javascript:;" data-val="blue" title="蓝色">蓝色</a></li>
                            <li><a href="javascript:;" data-val="green" title="绿色">绿色</a></li>
                            <li><a href="javascript:;" data-val="red" title="红色">红色</a></li>
                            <li><a href="javascript:;" data-val="yellow" title="黄色">黄色</a></li>
                            <li><a href="javascript:;" data-val="orange" title="橙色">橙色</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</header>
<!--/_header 作为公共模版分离出去-->

<!--_menu 作为公共模版分离出去-->
<aside class="Hui-aside" style="margin-top: 40px;">

    <div class="menu_dropdown bk_2">
        <dl id="menu-member">
            <dt><i class="Hui-iconfont">&#xe60d;</i> 个人中心<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
            <dd>
                <ul>
                    <li><a href="certcenter.html" title="个人认证">个人认证</a></li>
                </ul>
            </dd>
        </dl>
        <dl id="menu-tongji">
            <dt><i class="Hui-iconfont">&#xe61a;</i> 盈利统计<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
            <dd>
                <ul>
                    <li><a href="profit.html?sktype=1&start=0" title="支付宝盈利统计">支付宝盈利统计</a></li>
                    <li><a href="profit.html?sktype=2&start=0" title="微信盈利统计">微信盈利统计</a></li>
                </ul>
            </dd>
        </dl>
        <dl id="menu-system">
            <dt><i class="Hui-iconfont">&#xe62e;</i> 域名管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
            <dd>
                <ul>
                    <li><a href="urls.html" title="域名管理">域名管理</a></li>
                </ul>
            </dd>
        </dl>
    </div>
</aside>
<div class="dislpayArrow hidden-xs"><a class="pngfix" href="javascript:void(0);" onClick="displaynavbar(this)"></a>
</div>
<!--/_menu 作为公共模版分离出去-->

<section class="Hui-article-box">
    <nav class="breadcrumb"><i class="Hui-iconfont"></i> <a href="/user/index.html" class="maincolor">首页</a>
        <span class="c-999 en">&gt;</span>
        <span class="c-666">我的桌面</span>
        <a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px"
           href="javascript:location.replace(location.href);" title="刷新"><i class="Hui-iconfont">&#xe68f;</i></a></nav>
    <div class="Hui-article" style="margin-top: 40px;">
        <article class="cl pd-20">
            <p class="f-20 text-success">欢迎使用码农玖付-CodePay</p>
            <p>上次登录IP：222.35.131.79.1 上次登录时间：2014-6-14 11:19:55</p>
            <table class="table table-border table-bordered table-bg">
                <thead>
                <tr>
                    <th colspan="8" scope="col">收款统计: 总收入:(<font style="color: red;">${result.timemoney.alltime}元</font>)
                        周收入:(<font style="color: red;">${result.timemoney.weektiem}元</font>) 昨日收入:(<font
                                style="color: red;">${result.timemoney.yesterday}元</font>) 今日收入:(<font
                                style="color: red;">${result.timemoney.today}元</font>)
                    </th>
                </tr>
                <tr class="text-c">
                    <th>支付类型</th>
                    <th>增值类型</th>
                    <th>订单号</th>
                    <th>收款域名</th>
                    <th>收款金额</th>
                    <th>订单日期</th>
                    <th>收款日期</th>
                    <th>收款状态</th>
                </tr>


                </thead>
                <tbody>
                <c:forEach items="${result.receivables}" var="receivable">
                    <tr class="text-c">
                        <td><c:choose><c:when
                                test="${receivable.sktype==1}">支付宝</c:when><c:otherwise>微信</c:otherwise></c:choose></td>
                        <td><c:choose><c:when test="${receivable.skgdtype==1}">优惠</c:when><c:otherwise>惩罚</c:otherwise></c:choose></td>
                        <td>${receivable.skordernum}</td>
                        <td>${receivable.urlname}</td>
                        <td>${receivable.skmoney}</td>
                        <td>${receivable.createtime}</td>
                        <td>${receivable.sktime}</td>
                        <td><c:choose>
                            <c:when test="${receivable.skstate==1}">
                                <span
                                style="color: red;">代付款</span>
                            </c:when>
                            <c:when test="${receivable.skstate==2}">
                                <span
                                        style="color: green;">已付款</span>
                            </c:when>
                            <c:when test="${receivable.skstate==3}">
                                <span
                                        style="color:#ff7e0e;">已超时</span>
                            </c:when>
                        </c:choose>
                        </td>
                    </tr>
                </c:forEach>


                </tbody>
            </table>
        </article>
        <footer class="footer">
            <p>
                感谢jQuery、layer、laypage、Validform、UEditor、My97DatePicker、iconfont、Datatables、WebUploaded、icheck、highcharts、bootstrap-Switch<br>
                Copyright &copy;2015 H-ui.admin v3.0 All Rights Reserved.<br> 本后台系统由<a
                    href="http://www.h-ui.net/H-ui.admin.shtml" target="_blank"
                    title="H-ui.admin后端框架">H-ui.admin后端框架</a>提供后端技术支持</p>
        </footer>
    </div>
</section>

<!--_footer 作为公共模版分离出去-->
<script type="text/javascript" src="<%=request.getContextPath()%>/res/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/res/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/res/h-ui/js/H-ui.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/res/h-ui.admin/js/H-ui.admin.page.js"></script>
<!--/_footer /作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript">

</script>
<!--/请在上方写此页面业务相关的脚本-->

</body>
</html>
