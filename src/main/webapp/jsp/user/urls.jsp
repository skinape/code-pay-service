﻿<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <link rel="Bookmark" href="favicon.ico">
    <link rel="Shortcut Icon" href="favicon.ico"/>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="<%=request.getContextPath()%>/res/lib/html5.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/res/lib/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/res/h-ui/css/H-ui.min.css"/>
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/res/h-ui.admin/css/H-ui.admin.css"/>
    <link rel="stylesheet" type="text/css"
          href="<%=request.getContextPath()%>/res/lib/Hui-iconfont/1.0.8/iconfont.css"/>
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/res/h-ui.admin/skin/default/skin.css"
          id="skin"/>
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/res/h-ui.admin/css/style.css"/>
    <!--[if IE 6]>
    <script type="text/javascript" src="http://lib.h-ui.net/DD_belatedPNG_0.0.8a-min.js"></script>
    <script>DD_belatedPNG.fix('*');</script>
    <![endif]-->
    <!--/meta 作为公共模版分离出去-->

    <title>域名管理-个人后台</title>
    <meta name="keywords" content="H-ui.admin v3.0,H-ui网站后台模版,后台模版下载,后台管理系统模版,HTML后台模版下载">
    <meta name="description" content="H-ui.admin v3.0，是一款由国人开发的轻量级扁平化网站后台模板，完全免费开源的网站后台管理系统模版，适合中小型CMS后台系统。">
</head>
<body>
<!--_header 作为公共模版分离出去-->
<header class="navbar-wrapper">
    <div class="navbar navbar-fixed-top">
        <div class="container-fluid cl"><a class="logo navbar-logo f-l mr-10 hidden-xs" href="/">码农玖付-个人后台</a>
            <a class="logo navbar-logo-m f-l mr-10 visible-xs" href="/">CodePay</a>
            <a aria-hidden="false" class="nav-toggle Hui-iconfont visible-xs" href="javascript:;">&#xe667;</a>
            <nav id="Hui-userbar" class="nav navbar-nav navbar-userbar hidden-xs">
                <ul class="cl">
                    <li><a href="http://codepay.baldhome.com/CodePay.apk" style="color: red;">安卓APP下载(必下载)</a></li>
                    <li>${result.userinfo.vipname}</li>
                    <li class="dropDown dropDown_hover"><a href="#" class="dropDown_A">${result.userinfo.username} <i
                            class="Hui-iconfont">&#xe6d5;</i></a>
                        <ul class="dropDown-menu menu radius box-shadow">
                            <li><a href="javascript:;" onClick="myselfinfo()">个人信息</a></li>
                            <li><a href="#">退出</a></li>
                        </ul>
                    </li>
                    <li id="Hui-msg"><a href="#" title="消息"><span class="badge badge-danger">1</span><i
                            class="Hui-iconfont" style="font-size:18px">&#xe68a;</i></a></li>
                    <li id="Hui-skin" class="dropDown right dropDown_hover"><a href="javascript:;" class="dropDown_A"
                                                                               title="换肤"><i class="Hui-iconfont"
                                                                                             style="font-size:18px">&#xe62a;</i></a>
                        <ul class="dropDown-menu menu radius box-shadow">
                            <li><a href="javascript:;" data-val="default" title="默认（黑色）">默认（黑色）</a></li>
                            <li><a href="javascript:;" data-val="blue" title="蓝色">蓝色</a></li>
                            <li><a href="javascript:;" data-val="green" title="绿色">绿色</a></li>
                            <li><a href="javascript:;" data-val="red" title="红色">红色</a></li>
                            <li><a href="javascript:;" data-val="yellow" title="黄色">黄色</a></li>
                            <li><a href="javascript:;" data-val="orange" title="橙色">橙色</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</header>
<!--/_header 作为公共模版分离出去-->

<!--_menu 作为公共模版分离出去-->
<aside class="Hui-aside" style="margin-top: 40px;">

    <div class="menu_dropdown bk_2">
        <dl id="menu-member">
            <dt><i class="Hui-iconfont">&#xe60d;</i> 个人中心<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
            <dd>
                <ul>
                    <li><a href="certcenter.html" title="个人认证">个人认证</a></li>
                </ul>
            </dd>
        </dl>
        <dl id="menu-tongji">
            <dt><i class="Hui-iconfont">&#xe61a;</i> 盈利统计<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
            <dd>
                <ul>
                    <li><a href="profit.html?sktype=1&start=0" title="支付宝盈利统计">支付宝盈利统计</a></li>
                    <li><a href="profit.html?sktype=2&start=0" title="微信盈利统计">微信盈利统计</a></li>
                </ul>
            </dd>
        </dl>
        <dl id="menu-system">
            <dt><i class="Hui-iconfont">&#xe62e;</i> 域名管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
            <dd>
                <ul>
                    <li><a href="urls.html" title="域名管理">域名管理</a></li>
                </ul>
            </dd>
        </dl>
    </div>
</aside>
<div class="dislpayArrow hidden-xs"><a class="pngfix" href="javascript:void(0);" onClick="displaynavbar(this)"></a>
</div>
<!--/_menu 作为公共模版分离出去-->

<section class="Hui-article-box">
    <nav class="breadcrumb"><i class="Hui-iconfont"></i> <a href="/user/index.html" class="maincolor">首页</a>
        <span class="c-999 en">&gt;</span>
        <span class="c-666">域名管理</span>
        <a class="btn btn-success radius r btn-refresh" style="line-height:1.6em;margin-top:3px"
           href="javascript:location.replace(location.href);" title="刷新"><i class="Hui-iconfont">&#xe68f;</i></a></nav>
    <div class="Hui-article" style="margin-top: 40px;">
        <article class="cl pd-20 dataTables_wrapper">
            <div class="cl pd-5 bg-1 bk-gray mt-20">
                <span class="l"><a class="btn btn-primary radius" onclick="product_add('添加域名','url-add.html','800')"
                                   href="javascript:;"><i class="Hui-iconfont"></i> 添加域名</a></span></div>


            <table class="table table-border table-bordered table-bg">
                <thead>
                <tr class="text-c">
                    <th>域名ID</th>
                    <th>回调接口</th>
                    <th>域名别名</th>
                    <th>操作</th>
                </tr>


                </thead>
                <tbody>
                <c:forEach items="${result.urlinfo}" var="urlinfo">
                    <tr class="text-c">
                        <td>${urlinfo.urlid}</td>
                        <td>${urlinfo.urlname}</td>
                        <td>${urlinfo.urlalias}</td>
                        <td class="td-manage"><a style="text-decoration:none" class="ml-5"
                                                 onclick="product_edit('产品编辑','url-add.html',${urlinfo.urlid})"
                                                 href="javascript:;" title="编辑"><i class="Hui-iconfont"></i></a> <a
                                style="text-decoration:none" class="ml-5" onclick="product_del(this,${urlinfo.urlid})"
                                href="javascript:;" title="删除"><i class="Hui-iconfont"></i></a></td>
                    </tr>
                </c:forEach>


                </tbody>
            </table>
        </article>
        <footer class="footer">
            <p>
                感谢jQuery、layer、laypage、Validform、UEditor、My97DatePicker、iconfont、Datatables、WebUploaded、icheck、highcharts、bootstrap-Switch<br>
                Copyright &copy;2015 H-ui.admin v3.0 All Rights Reserved.<br> 本后台系统由<a
                    href="http://www.h-ui.net/H-ui.admin.shtml" target="_blank"
                    title="H-ui.admin后端框架">H-ui.admin后端框架</a>提供后端技术支持</p>
        </footer>

    </div>
</section>

<!--_footer 作为公共模版分离出去-->
<script type="text/javascript" src="<%=request.getContextPath()%>/res/lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/res/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/res/h-ui/js/H-ui.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/res/h-ui.admin/js/H-ui.admin.page.js"></script>
<!--/_footer /作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript">



    /*管理员-角色-添加*/
    function product_add(title,url,w,h){
        layer_show(title,url,w,h);
    }
    /*管理员-角色-编辑*/
    function product_edit(title,url,id,w,h){
        layer_show(title,url+"?urlid="+id,w,h);
    }


    /*图片-删除*/
    function product_del(obj, id) {
        layer.confirm('确认要删除吗？', function (index) {
            $.ajax({
                url:"daleteurl.json",
                type:"POST",
                datatype:"JSON",
                data:JSON.stringify({"urlid":id}),
                contentType: 'application/json',
                success:function (result) {
                    if(result==1){
                        $(obj).parents("tr").remove();
                        layer.msg('已删除!', {icon: 1, time: 1000});
                    }else{
                        layer.msg('删除失败!', {icon: 1, time: 1000});
                    }
                },

            });

        });
    }
</script>
<!--/请在上方写此页面业务相关的脚本-->

</body>
</html>