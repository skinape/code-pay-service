<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="format-detection" content="telephone=yes">
    <meta http-equiv="Cache-Control" content="no-transform">
	<link rel="shortcut icon" href="/res/images/favicon.ico" type="image/x-icon" />
    <meta http-equiv="Cache-Control" content="no-siteapp">
    <title>码农易支付-码农玖付、支付宝|微信免签约即时到帐、彩虹易支付、俩个接口快速对接</title>
    <meta name="description"
          content="码农玖付是一个免签约支付产品，支付金额直接到账你的商户,可以助你一站式解决网站签约各种支付接口的难题，现拥有支付宝、微信支付等免签约支付功能，并有开发文档SDK与视频，俩个接口即可快速集成到你的网站。"
          />
    <meta name="keywords" content="码农玖付 ,码农易支付,免签约支付系统,彩虹易支付,支付宝免签约即时到帐,微信免签约即时到帐,第三方免签约支付"/>
    <!--ico-->
    <link rel="stylesheet" type="text/css" href="res/css/base.css">
    <link rel="stylesheet" type="text/css" href="res/css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="res/css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="res/css/style.css">
    <link rel="stylesheet" type="text/css" href="res/css/responsive.css">
    <script src="/res/js/jquery-1.8.3.js"></script>
    <script src="/res/js/wow.min_1.js"></script>
    <script src="/res/js/owl.carousel.min.js"></script>
    <script src="/res/js/page.js"></script>
    <style type="text/css">
        <!--
        .STYLE1 {
            color: #FF0000
        }

        -->
    </style>
</head>
<body>
<div class="header">
    <div class="container">
        <div class="rowFluid">
            <div class="span3 col-md-12">
                <div class="logo"><a href="/"><img src="/res/images/logo.png"/></a></div>
            </div>
            <div class="span9">
                <div class="mobileMenuBtn"><span class="span1"></span><span class="span2"></span><span
                        class="span3"></span></div>
                <div class="mobileMenuBtn_shad"></div>
                <div class="header_menu">
                    <ul id="menu">
                        <li><a href="/">首页</a></li>
                        <li><a href="#fw">平台优势</a></li>
                        <li><a href="#ys">三大功能</a></li>
                        <li><a href="#gy">关于我们</a></li>
                        <li><a href="/demo.html">在线测试</a></li>
                        <li><a href="/doc.html">开发文档</a></li>
                        <li><a href="/user/reg.html" target="_blank">商户注册</a></li>

                    </ul>
                </div>
            </div>
            <div class="span2"></div>
        </div>
    </div>
</div>


</div>
<div class="page">
    <div class="rowFluid">
        <div class="span12">
            <div class="main">
                <div class="banner">
                    <div class="rowFluid">
                        <div class="span12">
                            <div class="owl-demo  wow fadeInUp">
                                <div class="item">
                                    <h3 class="banner_title">码农玖付/CodePay</h3><br>

                                    <div class="banner_jianjie">

                                        <div class="banner_text">俩个接口，完成需求站支付业务</div>
                                        <div class="banner_text">无需营业执照&nbsp;无需提现&nbsp;无需电脑挂机&nbsp;无需登录支付宝/微信账号</div>
                                        <div class="banner_text">"不三不四"聚合支付系统&nbsp;&nbsp;新型即时到账接口</div>
										<div class="banner_text">支持支付宝免签约即时到帐|支持微信免签约即时到帐</div>
                                    </div>
                                    <div class="banner_text">&nbsp;</div>
                                    <div class="banner_text">&nbsp;</div>
                                    <div class="banner_button"><a href="/user/login.html" target="_blank">商户登录</a>
                                    </div>
                                </div>
                            </div>
                            <ul class="platform_advantage_bg">
                                <li></li>
                                <li></li>
                                <li></li>
                                <li></li>
                                <li></li>
                                <li></li>
                                <li></li>
                                <li></li>
                                <li></li>
                                <li></li>
                            </ul>
                        </div>
                    </div>
                    <div id="container" class="mpage">
                        <div id="anitOut" class="anitOut"></div>
                    </div>
                </div>
                <div class="kzf-mod-product" id="fw">
                    <div class="rowFluid">
                        <div class="container">
                            <div class="kzf-mod-product-header ">
                                <h3 class="kzf-mod-product-title">我们的优势</h3>
                                <span>方便快捷的支付接入体验，让支付和收款更简单！</span>
                            </div>
                            <div class="kzf-mod-product-content">
                                <div class="span4 col-sm-6">
                                    <div class="kzf-mod-product-list " style="height:  126px;">
                                        <div class="kzf-mod-product-list-title"><img src="res/images/081.png"
                                                                                     alt="极简使用">极简使用
                                        </div>
                                        <p>俩个接口，极速完成，支付接入 简洁的操作界面易于使用</p>
                                    </div>
                                </div>
                                <div class="span4 col-sm-6">
                                    <div class="kzf-mod-product-list ">
                                        <div class="kzf-mod-product-list-title"><img src="res/images/082.png"
                                                                                     alt="灵活便利">灵活便利
                                        </div>
                                        <p>产品服务灵活组合 满足企业多元化业务需求</p>
                                    </div>
                                </div>
                                <div class="span4 col-sm-6">
                                    <div class="kzf-mod-product-list " style="height:  126px;">
                                        <div class="kzf-mod-product-list-title"><img src="res/images/086.png"
                                                                                     alt="不介入资金流">不介入资金流
                                        </div>
                                        <p>只负责交易处理不参与资金清算 保障您的资金安全</p>
                                    </div>
                                </div>
                                <div class="span4 col-sm-6">
                                    <div class="kzf-mod-product-list ">
                                        <div class="kzf-mod-product-list-title"><img src="res/images/090.png"
                                                                                     alt="大数据">大数据
                                        </div>
                                        <p>运用交易数据分析功能 了解公司运营状况</p>
                                    </div>
                                </div>
                                <div class="span4 col-sm-6">
                                    <div class="kzf-mod-product-list ">
                                        <div class="kzf-mod-product-list-title"><img src="res/images/092.png"
                                                                                     alt="增值服务">增值服务
                                        </div>
                                        <p>提供金融产品及技术服务 帮助企业整合互联网金融</p>
                                    </div>
                                </div>
                                <div class="span4 col-sm-6">
                                    <div class="kzf-mod-product-list ">
                                        <div class="kzf-mod-product-list-title"><img src="res/images/087.png"
                                                                                     alt="安全稳定">安全稳定
                                        </div>
                                        <p>平台独自运行于超强服务器上 多备份容灾保障</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="platform_advantage" id="ys">
                    <div class="rowFluid">
                        <div class="span12">
                            <div class="container">
                                <div class="all_title2">
                                    <h3 class="title">软件截图</h3>
                                </div>
                                <div class="cloud_platform_content">
                                 <img style="width:50%;float: left;" src="res/images/yemian1.jpg" data-url="#">
		                         <img style="width:50%;float: left;" src="res/images/yemian2.jpg" data-url="#">
	
                                 <img style="width:50%;height:30%;float: left;" src="res/images/shouji1.png" data-url="#">
		                         <img style="width:50%;height:30%;float: left;" src="res/images/shouji2.png" data-url="#">
                                </div>
                            </div>
                            <ul class="platform_advantage_bg">
                                <li></li>
                                <li></li>
                                <li></li>
                                <li></li>
                                <li></li>
                                <li></li>
                                <li></li>
                                <li></li>
                                <li></li>
                                <li></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="kzf-mod-case-container" id="gy">
                    <div class="rowFluid">
                        <div class="container">
                            <div class="span12">
                                <div class="kzf-mod-case-out">
                                    <div class="kzf-mod-so-title-box ">
                                        <h3 class="kzf-so-title">关于我"们"</h3>
                                    </div>
                                    <div class="kzf-mod-case-box ">
                                        <div class="span6 col-sm-12"><a>
                                            <div class="kzf-mod-wrap ">
                                                <div class="kzf-mod-pic"><img src="res/images/666.png"
                                                                              alt="某某财务公司形象"></div>
                                            </div>
                                        </a></div>
                                        <div class="span6 col-sm-12">
                                            <div class="kzf-mod-wrap ">
                                                <div class="kzf-mod-title">
                                                    <p><strong>很平凡，很努力</strong><br>
                                                        专业的技术团队与丰富的行业资源保障稳定、便捷的产品和服务。通过SDK接入多种主流通道，用saas模式创建商户交易管理系统，您无须投入技术团队和通道资源也能够实现自己的支付系统、管理自己的交易订单。<br>
                                                        <strong>便捷的商户管理平台</strong><br>
                                                        解忧云支付开放平台——商户支付管理系统。它能够满足企业集中式管理支付应用，进行快速的财务对账，便捷的交易与结算查询，更有商业智能BI帮助企业分析经营。<br>
                                                        <strong>简便的全流程自助服务</strong><br>
                                                        我们提倡开放便捷共享，平台提供方便简易的入网申请、在线接口联调、测试上线等自助式操作功能，化繁为简，一杯咖啡时间帮助你接入支付。同时平台全面提供7*24小时客户服务，保障客户问题随时得到处理解决。<br>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="join_in">
                    <div class="rowFluid">
                        <div class="span12">
                            <div class="container">
                                <div class="join_in_title "><span>码农玖付</span>，做您身边最稳定,安全的支付服务商</div>
                                <div class="join_in_text ">选择优质稳定支付能让您更快更方便的接入第三方支付时代</div>
                                <a href="/user/reg.html" class="all_button join_in_button " target="_blank">商户注册</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="partners">
                    <div class="rowFluid">
                        <div class="span12">
                            <div class="container">
                                <div class="all_title1 ">
                                    <h3 class="title">合作伙伴</h3>
                                </div>


                                <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                    <img class="img-COOP" src="/res/images/cooprate1_1.jpg" alt="">
                                </div>
                                <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                    <img class="img-COOP" src="/res/images/cooprate1_2.jpg" alt="">
                                </div>

                            </div>
                        </div>
                    </div>
                </div>


                <div>
                </div>
            </div>
        </div>
        <div class="font-s12 push-20 clearfix">
            <hr class="remove-margin-t">
            <span style="font-size:13px">
<font color=Magenta><center>

  <a href="/" class="all_button join_in_button " target="_blank">Copyright © 2019 码农玖付</a></div>
        </ul>
    </div>
</div>
</div>

<div class="span4 col-xm-6 col-xs-12 text_align">
    <div class="footer_list">
        <div class="footer_cotact">
            <div class="footer_cotact_title"></div>
            <ul>

            </ul>
        </div>
    </div>
</div>
</div>
</div>


</span>

</div>

</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</body>


</html>



