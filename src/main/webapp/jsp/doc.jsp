<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8"/>
    <title>Skin Ape-码农玖付开发文档 - JAVA|PHP|易语言|.NET 免签约支付即时到账开发文档</title>
    <meta name="description"
          content="码农玖付是一个免签约支付产品，支付金额直接到账你的商户,可以助你一站式解决网站签约各种支付接口的难题，现拥有支付宝、微信支付等免签约支付功能，并有开发文档SDK与视频，俩个接口即可快速集成到你的网站。"
    />
    <meta name="keywords" content="码农玖付 ,码农易支付,免签约支付系统,彩虹易支付,支付宝免签约即时到帐,微信免签约即时到帐,第三方免签约支付"/>
    <meta name="viewport" content="user-scalable=no, width=device-width">
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="shortcut icon" href="/res/images/favicon.ico" type="image/x-icon" />
    <meta name="renderer" content="webkit">
    <link rel="stylesheet"
          href="https://template.down.swap.wang/ui/angulr_2.0.1/bower_components/font-awesome/css/font-awesome.min.css"
          type="text/css"/>
    <link rel="stylesheet"
          href="https://template.down.swap.wang/ui/angulr_2.0.1/bower_components/bootstrap/dist/css/bootstrap.css"
          type="text/css"/>
    <link rel="stylesheet" href="https://template.down.swap.wang/template/spay/common.css">
    <link rel="stylesheet" href="https://template.down.swap.wang/template/spay/index-top.css">
    <!--[if IE 9 ]>
    <style type="text/css">#ie9 {
        display: block;
    }</style><![endif]-->
    <script src="https://template.down.swap.wang/ui/angulr_2.0.1/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="https://template.down.swap.wang/ui/angulr_2.0.1/bower_components/bootstrap/dist/js/bootstrap.js"></script>
    <script src="https://template.down.swap.wang/template/spay/jquery-ujs.js" async="true"></script>
    <link rel="stylesheet" type="text/css" href="https://template.down.swap.wang/template/spay/index.css"/>
    <style type="text/css">body {
        color: #000;
    }

    header {
        position: relative;
    }</style>
</head>
<body>
<!--[if (gt IE 6)&amp;(lt IE 9)]>
<h1 style='color:red;text-align:center;'>
      你好，浏览器版本过低，升级可正常访问,点击<a style="color:blue"href="http://browsehappy.com/">升级您的浏览器</a>
</h1>
<style type="text/css">#ielt9{ display: none; }h1{ height:300px;line-height: 300px;display:block; }header{ display: none; }#ie9{ display: block; }.tenxcloud-logo{ margin:50px auto 0;display:block}</style>
<![endif]-->

<script type="text/javascript">

    function aclos() {
        document.getElementById("q_Msgbox").style.display = "none";
    }
</script>


<div id="ielt9" style="height:100%">


    <div id="ie9">你当前的浏览器版本过低，请您升级至IE9以上版本，以达到最佳效果，谢谢！<span class="closeIE">X</span></div>
    <div id="scroll_Top">
        <i class="fa fa-arrow-up"></i>
        <a href="javascript:;" title="去顶部" class="TopTop">TOP</a></div>
    <script>

        $('.closeIE').click(function (event) {
            $('#ie9').fadeOut();
        });
    </script>

    <style type="text/css">
        .bann {
            content: '';
            background-size: 100%;
            background: #4280cb;
            background: -webkit-gradient(linear, 0 0, 0 100%, from(#4585d2), to(#4280cb));
            background: -moz-linear-gradient(top, #4585d2, #4280cb);
            background: linear-gradient(to bottom, #4585d2, #4280cb);
            top: 0;
            left: 0;
            z-index: -1;
            min-height: 50px;
            width: 100%
        }

        .fl .active {
            color: #3F5061;
            background: #fff;
            border-color: #fff
        }
    </style>

    <div class="bann">
        <div class="logo"><a href="http://codepay.baldhome.com"><img src="/res/images/logo.png"/></a></div>

        <div class="col-xs-12" style="text-align:center;">
            <div class="h3" style="color:#ffffff;margin-top: 35px;margin-bottom: 30px;">开发文档<span style='font-size:13px;color:red;'>作者:Skin Ape</span></div>

            <div style="clear:both;"></div>
        </div>
        <div style="clear:both;"></div>
    </div>


    <div class="container">

        <!-- Docs nav
        ================================================== -->
        <div class="row">
            <div class="col-md-3 ">
                <div id="toc" class="bc-sidebar">
                    <ul class="nav">
                        <hr/>
                        <li class="toc-h2"><a href="#sdk0">SDK文档下载</a></li>
                        <hr/>
                        <li class="toc-h2 toc-active"><a href="#toc0">支付接口介绍</a></li>
                        <li class="toc-h2"><a href="#toc1">接口申请方式</a></li>
                        <li class="toc-h2"><a href="#toc2">协议规则</a></li>
                        <hr/>
                        <li class="toc-h2"><a href="#api0">[API]初始化</a></li>

                        <li class="toc-h2"><a href="#api2">[API]创建订单</a></li>
                        <li class="toc-h2"><a href="#api3">[API]查询订单状态</a></li>
                        <li class="toc-h2"><a href="#api4">[API]查询营业金额</a></li>
                        <hr/>
                        <li class="toc-h2"><a href="#pay0">收款成功码农玖付回调</a></li>

                    </ul>
                </div>
            </div>

            <div class="col-md-9">
                <article class="post page">
                    <section class="post-content">
                        <h3 id="sdk0">SDK下载JAVA版</h3>
                        <blockquote>
                            <a href="./MN_JAVA_Demo.zip" style="color:blue">MN_JAVA_Demo.zip</a><br/>
                        </blockquote>
                        <h3 id="sdk0">SDK下载PHP版&nbsp;<span style='font-size:15px;color:red;'>(由QQ:47104041提供)</span></h3>
                        <blockquote>
                            <a href="./MN_PHP_Demo.zip" style="color:blue">MN_PHP_Demo.zip</a><br/>
                        </blockquote>
                        <h3 id="sdk0">SDK下载Python版&nbsp;<span style='font-size:15px;color:red;'>(由QQ:714647751提供)</span></h3>
                        <blockquote>
                            <a href="./MN_Python_Demo.zip" style="color:blue">MN_Python_Demo.zip <span style="color: red;">已把获取可用金额废掉,里面的代码未改</span></a><br/>
                        </blockquote>
                        <h3 id="sdk0">SDK下载易语言版</h3>
                        <blockquote>
                            <a href="./MN_E_Demo.zip" style="color:blue">MN_E_Demo.zip</a><br/>
                        </blockquote>
                        <h3 id="sdk0">易安卓、ASP、C#。。</h3>
                        <blockquote>
                            <span style="color:blue">后面陆续开发。欢迎广大开发者提供,提供Demo者免费永久使用本平台</span><br/><br/>
                            <span style="color:blue">开发的时候要取码农服务器IP<span style="color:red">(这样访问速度快!)</span>，直接GET就可以，然后GET到的<span style="color:red">IP+:8081(也就是端口)。</span>GET地址:<span style="color:red">http://www.baldhome.com/ip.txt</span></span><br/><br/>
                            <a href="http://wpa.qq.com/msgrd?v=3&uin=47104041&site=qq&menu=yes" style="color:red">点我投稿</a><br/>
                        </blockquote>

                        <h2 id="toc0">码农玖付支付接口介绍</h2>
                        <blockquote><p>开发初衷:自己本身是一个程序员，但是没有资格去申请官方支付功能，所以码农玖付诞生 了！！。使用此接口可以实现支付宝、微信支付的即时到账，免签约，无需交钱，无需企业认证。<br>接口API地址是：http://codepay.baldhome.com/</p>
                        </blockquote>
                        <p>本文阅读对象：商户系统（在线购物平台、人工收银系统、自动化智能收银系统或其他）集成码农玖付支付涉及的技术架构师，研发工程师，测试工程师，系统运维工程师。</p>
                        <h2 id="toc1">接口申请方式</h2>
                        <p>请前往官网：http://codepay.baldhome.com/进行注册一个账号，后台设置一下，代码配置一下</p>
                        <h2 id="toc2">协议规则</h2>
                        <p>传输方式：HTTP</p>
                        <p>请求数据格式：POST</p>
                        <p>返回数据格式：JSON</p>
                        <p>签名算法：不告诉你</p>
                        <p>字符编码：UTF-8</p>
                        <hr/>
                        <h2 id="api0">[API]初始化</h2>
                        <h2 id="api0" style="color:red;">特别注意:下面每个接口一定要共享cookie</h2>
                        <p>API权限：所有人</p>
                        <p>URL地址：<span style="color:red;">http://codepay.baldhome.com/login.json</span></p>
                        <p>返回数据(参考)：{"result":{"vipname":"终身会员","userbh":"admin","userkey":"Tmbnn7KkxD9J63TXh8MKRw==","username":"管理员"}}</p>
                        <p>请求参数说明(POST)：</p>
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>字段名</th>
                                <th>变量名</th>
                                <th>必填</th>
                                <th>类型</th>
                                <th>示例值</th>
                                <th>描述</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>用户编号</td>
                                <td>userbh</td>
                                <td>是</td>
                                <td>String</td>
                                <td>admin</td>
                                <td>注册的登陆账号</td>
                            </tr>
                            <tr>
                                <td>用户密码</td>
                                <td>userpwd</td>
                                <td>是</td>
                                <td>String</td>
                                <td>admin</td>
                                <td>注册的登陆密码</td>
                            </tr>
                            <tr>
                                <td>代码登录标识</td>
                                <td>service</td>
                                <td>是</td>
                                <td>int</td>
                                <td>1</td>
                                <td>代码登录标识</td>
                            </tr>
                            <tr>
                                <td>返回数据样式</td>
                                <td>resultype</td>
                                <td>是</td>
                                <td>String</td>
                                <td>json</td>
                                <td>json/字符串</td>
                            </tr>
                            </tbody>
                        </table>
                        <p>返回结果(JSON)：</p>
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>字段名</th>
                                <th>变量名</th>
                                <th>类型</th>
                                <th>示例值</th>
                                <th>描述</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>会员类型</td>
                                <td>vipname</td>
                                <td>String</td>
                                <td>终身会员</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>用户编号</td>
                                <td>userbh</td>
                                <td>String</td>
                                <td>admin</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>用户名称</td>
                                <td>username</td>
                                <td>String</td>
                                <td>管理员</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>码农密钥</td>
                                <td>userkey</td>
                                <td>String</td>
                                <td>Tmbnn7KkxD9J63TXh8MKRw==</td>
                                <td>用户码农玖付回调验证秘钥</td>
                            </tr>
                            </tbody>
                        </table>

                        <h2 id="api2">[API]创建订单</h2>
                        <p>API权限：初始化成功</p>
                        <p>URL地址：<span style="color:red;">http://codepay.baldhome.com/createorder.json</span></p>
                        <p>返回数据(参考)：0.85</p>
                        <p style="color:red;font-size:18px">注意:在码农服务器创建好订单也需要在自己的数据库创建订单!! 前端页面不停的查询自己的数据库订单状态!! 如果客户支付成功会通过<a href="#pay0" style="color:blue;">收款成功码农玖付回调</a> 调用你自己的接口进行反写订单状态</p>
                        <p>请求参数说明(POST)：</p>
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>字段名</th>
                                <th>变量名</th>
                                <th>必填</th>
                                <th>类型</th>
                                <th>示例值</th>
                                <th>描述</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>用户编号</td>
                                <td>userbh</td>
                                <td>是</td>
                                <td>String</td>
                                <td>admin</td>
                                <td>此API固定值</td>
                            </tr>
                            <tr>
                                <td>绑定站点</td>
                                <td>urltype</td>
                                <td>是</td>
                                <td>Int</td>
                                <td>1</td>
                                <td>在后台获取绑定站点对应ID</td>
                            </tr>
                            <tr>
                                <td>支付金额</td>
                                <td>money</td>
                                <td>是</td>
                                <td>double</td>
                                <td>0.85</td>
                                <td>商品实际价格(必须为整数)</td>
                            </tr>
                            <tr>
                                <td>支付类型</td>
                                <td>paytype</td>
                                <td>是</td>
                                <td>int</td>
                                <td>1</td>
                                <td>1.支付宝 2.微信</td>
                            </tr>
                            <tr>
                                <td>增值类型</td>
                                <td>gdtype</td>
                                <td>是</td>
                                <td>int</td>
                                <td>1</td>
                                <td>1.优惠(在基础价格上优惠) 2.惩罚(在基础价格上加钱)</td>
                            </tr>
                            <tr>
                                <td>订单号</td>
                                <td>ordernum</td>
                                <td>是</td>
                                <td>String</td>
                                <td>2019030290160340510</td>
                                <td>这个订单号唯一值，用户订单生成</td>
                            </tr>
                            <tr>
                                <td>返回数据样式</td>
                                <td>resultype</td>
                                <td>是</td>
                                <td>String</td>
                                <td>json</td>
                                <td>json/字符串</td>
                            </tr>
                            </tbody>
                        </table>
                        <p>返回结果(JSON)：</p>
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>字段名</th>
                                <th>变量名</th>
                                <th>类型</th>
                                <th>示例值</th>
                                <th>描述</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>用户编号</td>
                                <td>userbh</td>
                                <td>String</td>
                                <td>admin</td>
                                <td>此API固定值</td>
                            </tr>
                            <tr>
                                <td>绑定站点</td>
                                <td>urltype</td>
                                <td>Int</td>
                                <td>1</td>
                                <td>在后台获取绑定站点对应ID</td>
                            </tr>

                            <tr>
                                <td>支付类型</td>
                                <td>paytype</td>
                                <td>int</td>
                                <td>1</td>
                                <td>1.支付宝 2.微信</td>
                            </tr>
                            <tr>
                                <td>增值类型</td>
                                <td>gdtype</td>
                                <td>int</td>
                                <td>1</td>
                                <td>1.优惠(在基础价格上优惠) 2.惩罚(在基础价格上加钱)</td>
                            </tr>
                            <tr>
                                <td>订单号</td>
                                <td>ordernum</td>
                                <td>String</td>
                                <td>2019030290160340510</td>
                                <td>这个订单号唯一值，用户订单生成</td>
                            </tr>
                            <tr>
                                <td>返回数据样式</td>
                                <td>resultype</td>
                                <td>String</td>
                                <td>json</td>
                                <td>json/字符串</td>
                            </tr>
                            <tr style="color:red;">
                                <td >创建状态</td>
                                <td>state</td>
                                <td>Int</td>
                                <td>1</td>
                                <td>1为成功，1006为订单号重复,1007为未实名认证</td>
                            </tr>
                            <tr style="color:red;">
                                <td >客户需要支付金额</td>
                                <td>money</td>
                                <td>double</td>
                                <td>0.85</td>
                                <td>返回:基础金额+(0.01到0.99) 可用的金额,这个需要显示在前端界面上</td>
                            </tr>
                            </tbody>
                        </table>

                        <h2 id="api3">[API]查询订单状态</h2>
                        <p>URL地址：待开发</p>
                        <p>请求参数说明：</p>
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>字段名</th>
                                <th>变量名</th>
                                <th>必填</th>
                                <th>类型</th>
                                <th>示例值</th>
                                <th>描述</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                        <p>返回结果：</p>
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>字段名</th>
                                <th>变量名</th>
                                <th>类型</th>
                                <th>示例值</th>
                                <th>描述</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>

                        <h2 id="api4">[API]查询营业金额</h2>
                        <p>URL地址：待开发</p>
                        <p>请求参数说明：</p>
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>字段名</th>
                                <th>变量名</th>
                                <th>必填</th>
                                <th>类型</th>
                                <th>示例值</th>
                                <th>描述</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                        <p>返回结果：</p>
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>字段名</th>
                                <th>变量名</th>
                                <th>类型</th>
                                <th>示例值</th>
                                <th>描述</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>

                        <h2 id="pay0" style="color:red;">收款成功码农玖付回调，很重要！很重要！很重要！</h2>
                        <p>说明:码农玖付收款成功，会向商家发送成功状态，商家需要提供一个接收数据的接口<span style="color:red;">(接口的返回地址就是后台绑定的域名地址)</span></p>
                        <p>回调参数说明(POST)：</p>
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>字段名</th>
                                <th>变量名</th>
                                <th>类型</th>
                                <th>示例值</th>
                                <th>描述</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>支付类型</td>
                                <td>sktype</td>
                                <td>int</td>
                                <td>2</td>
                                <td>1.支付宝 2.微信</td>
                            </tr>
                            <tr>
                                <td>增值类型</td>
                                <td>skgdtype</td>
                                <td>int</td>
                                <td>1</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>收款金额</td>
                                <td>skmoney</td>
                                <td>Double</td>
                                <td>0.67</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>回调秘钥</td>
                                <td>manongkey</td>
                                <td>String</td>
                                <td>Tmbnn7KkxD9J63TXh8MKRw==</td>
                                <td>通过这个秘钥判断是不是码农玖付官方，通过<a href="#api0" style="color:red;">[API]初始化</a> 获取的</td>
                            </tr>
                            <tr>
                                <td>订单号</td>
                                <td>skordernum</td>
                                <td>String</td>
                                <td>2019030290160340510</td>
                                <td>收款成功的订单号,可以通过这个去更新数据库的订单状态</td>
                            </tr>
                            <tr>
                                <td>收款时间</td>
                                <td>time</td>
                                <td>String</td>
                                <td>2019-03-30+14:12:15.0</td>
                                <td>时间</td>
                            </tr>
                            </tbody>
                        </table>
                        <hr/>


                    </section>
                </article>
            </div>
        </div>

    </div>


    <div class="address">
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-8 col-lg-9">
                        <ul class="porduct">
                            <h4>合作伙伴</h4>
                            <li><a href="http://www.baldhome.com" target="_blank">程序员之家</a></li>
                            、
                        </ul>
                        <ul class="price">
                            <h4>关于我们</h4>
                            <li>码农玖付</li>
                        </ul>
                        <ul class="about" style="width: 40%;padding-left: 22px;">
                            <h4>联系我们</h4>
                            <li><strong>QQ:</strong><a target="_blank"
                                                       href="http://wpa.qq.com/msgrd?v=3&amp;uin=47104041&amp;site=qq&amp;menu=yes">47104041</a>
                            </li>
                            <li><strong>Email:</strong><a name="baidusnap4"></a><a href="mailto:47104041@qq.com">47104041@qq.com</a>
                            </li>
                        </ul>
                    </div>

                </div>
                <div class="xinxi">
                    <p>Copyright © 2019 码农玖付 | Powered by <a href="http://codepay.baldhome.com/">codepay.baldhome.com</a></p>
                </div>

            </div>
        </footer>
    </div>
    <script>
        (function(){
            var bp = document.createElement('script');
            var curProtocol = window.location.protocol.split(':')[0];
            if (curProtocol === 'https') {
                bp.src = 'https://zz.bdstatic.com/linksubmit/push.js';
            }
            else {
                bp.src = 'http://push.zhanzhang.baidu.com/push.js';
            }
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(bp, s);
        })();
    </script>

</div>
</body>
</html>
