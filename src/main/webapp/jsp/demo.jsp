<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <title>码农玖付-支付宝|微信免签约即时到账充值接口</title>
	<meta name="description"
          content="码农玖付是一个免签约支付产品，支付金额直接到账你的商户,可以助你一站式解决网站签约各种支付接口的难题，现拥有支付宝、微信支付等免签约支付功能，并有开发文档SDK与视频，俩个接口即可快速集成到你的网站。"
         />
    <meta name="keywords" content="码农玖付 ,码农易支付,免签约支付系统,彩虹易支付,支付宝免签约即时到帐,微信免签约即时到帐,第三方免签约支付"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="//cdn.bootcss.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://css.letvcdn.com/lc04_yinyue/201612/19/20/00/bootstrap.min.css">
    <link rel="shortcut icon" href="/res/images/favicon.ico" type="image/x-icon" />

</head>
<body>
<div class="logo"><a href="http://codepay.baldhome.com"><img src="/res/images/logo.png"/></a></div>
    <div class="container" style="padding-top:70px;">
        <center>
            <div>
                <button type="radio" name="type" value="wxpay" class="btn btn-info">码农玖付--付款以二维码下面为准！！</button>
                <div class="col-xs-12 col-sm-10 col-lg-8 center-block" style="float: none;">
                    <div class="panel panel-primary">

                        <div class="panel-body">
                            <form name=codepayfrom action="" method="post">
                                <div class="input-group">

                                    <span class="input-group-addon"><span
                                            class="glyphicon glyphicon-barcode"></span></span>

                                    <input size="30" name="ordernum" readonly="readonly" value="20190320125711374"
                                           class="form-control" placeholder="商户订单号"/>
                                </div>
                                <br/>
                                <div class="input-group">
                                    <span class="input-group-addon"><span
                                            class="glyphicon glyphicon-shopping-cart"></span></span>
                                    <input size="30" readonly="readonly" value="码农玖付" class="form-control"
                                           placeholder="码农玖付" required="required"/>
                                </div>
                                <br/>
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-yen"></span></span>
                                    <input size="30" readonly="readonly" name="money" value="1" class="form-control"
                                           placeholder="付款金额" required="required"/>
                                </div>
                                <br/>
                                <div class="input-group" style="text-align: left;">
                                    <span class="input-group-addon" style="color: red;"> 支付方式(必选):</span>
                                    <input type="radio" checked="checked" name="paytype" value="1" />支付宝
                                    <input type="radio" name="paytype" value="2" />微信
                                </div>
                                <div class="input-group" style="text-align: left;">
                                    <span class="input-group-addon" style="color: red;"> 增值选项(必选):</span>
                                    <input type="radio" checked="checked" name="gdtype" value="1" />优惠价格（基于付款金额减钱）
                                    <input type="radio" name="gdtype" value="2" />惩罚价格（基于付款金额加钱）
                                </div>
                                <div class="input-group" id="erweima">

                                </div>
                                <br/>
                                <center>
                                    <div class="btn-group btn-group-justified" role="group" aria-label="...">
                                        <div class="btn-group" role="group">
                                            <button onclick="return  func()" class="submit btn btn-primary">
                                               提交
                                            </button>
                                        </div>
                                    </div>
                                </center>
                            </form>
                            <p style="text-align:center"><br>如未充值到账，付款后请立即联系商家！</a>!</p>
                        </div>
                        <p style="text-align:center"><br>无需提现!无需手续费!</p>
                    </div>
                    <p style="text-align:center"><br>&copy; Powered by <a href="http://codepay.baldhome.com">码农玖付</a>!</p>
                </div>
            </div>
        </center>
    </div>
    <%--<script src="//cdn.bootcss.com/jquery/1.11.3/jquery.min.js"></script>--%>
    <%--<script src="//cdn.bootcss.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>--%>
    <script src="/res/js/jquery-1.8.3.js"></script>
    <script>
        //状态
        var state;
        //订单号
        var ordernum;
        var timeid;

        $(function () {
            //赋订单号值
            $("[name=ordernum]").val(randomNumber());

            //提交
            $(".submit").click(function () {
                var data=JSON.stringify($('[name=codepayfrom]').serializeObject());
                $.ajax({
                    url:"/submitorder.json",
                    type:"POST",
                    datatype:"JSON",
                    data:data,
                    contentType: 'application/json',
                    success:function (result) {
                        state=result.result.state;
                        ordernum=result.result.ordernum;
                        if(state==1){
                            //1为支付宝
                            if(result.result.paytype==1){
                                $("#erweima").append("<img  style='width: 200px;' src='/res/images/zhifubao.jpg'><p style='color: red;'>你需要支付:"+result.result.money+"元</p>");
                            }else{
                                $("#erweima").append("<img  style='width: 200px;' src='/res/images/weixin.jpg'><p style='color: red;'>你需要支付:"+result.result.money+"元</p>");
                            }
                            timeid=setInterval("showAuto()", 1000);
                        }else if(state==1006){
                            alert("订单号重复");
                        }


                    },

                });

            });


        });

        //监视是否充值成功
        function showAuto(){
            if(state!=undefined&&state==1){
                $.ajax({
                    url:"/getorderstate.json",
                    type:"POST",
                    datatype:"JSON",
                    data:"{\"ordernum\":\""+ordernum+"\"}",
                    contentType: 'application/json',
                    success:function (result) {
                        if (result.result.state==2){
                            clearInterval(timeid);
                            $("#erweima").html("");
                            alert("充值成功！");
                            //赋订单号值
                            $("[name=ordernum]").val(randomNumber());
                        }
                    },

                });
            }

        }

        //   根据当前时间和随机数生成流水号
        function randomNumber() {
            var now = new Date();
            var year = now.getFullYear();       //年
            var month = now.getMonth() + 1;     //月
            var day = now.getDate();            //日
            var hh = now.getHours();            //时
            var mm = now.getMinutes();          //分
            var ss = now.getSeconds();           //秒
            var clock = year + "";
            if(month < 10)
                clock += "0";

            clock += month + "";

            if(day < 10)
                clock += "0";

            clock += day + "";

            if(hh < 10)
                clock += "0";

            clock += hh + "";
            if(mm < 10) clock += '0';
            clock += mm + "";

            if(ss < 10) clock += '0';
            clock += ss;

            var outTradeNo="";  //订单号
            for(var i=0;i<3;i++) //6位随机数，用以加在时间戳后面。
            {
                outTradeNo += Math.floor(Math.random()*10);
            }
            clock+=outTradeNo;
            return(clock);
        }
        function func() {
            return false;
        }

    </script>
</body>
</html>